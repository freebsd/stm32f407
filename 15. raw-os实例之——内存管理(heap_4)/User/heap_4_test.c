/*
		heap_4内存分配演示	

*/

#include <stdio.h>
#include <stdarg.h>
#include <stm32f4xx_conf.h>
#include <lib_string.h>
#include "raw_api.h"
#include "shell_task.h"
#include "USART1.h"
#include <mm/heap_4_config.h>


#define TASK_STACK_SIZE   			256		// 任务栈大小

PORT_STACK Task1_Stack[TASK_STACK_SIZE];	// 任务栈
PORT_STACK Task2_Stack[TASK_STACK_SIZE];
PORT_STACK Task3_Stack[TASK_STACK_SIZE];

RAW_TASK_OBJ 		Task1;					// 任务对像
RAW_TASK_OBJ 		Task2;
RAW_TASK_OBJ 		Task3;


// 以下四个变量在key.c中定义
extern RAW_SEMAPHORE	Key0_Semaphore;		// 按键0同步信号量
extern RAW_SEMAPHORE	Key1_Semaphore;		// 按键1同步信号量
extern RAW_SEMAPHORE	Key2_Semaphore;		// 按键2同步信号量
extern RAW_TIMER      	Key_Timer;			// 周期性检测按键定时器
extern RAW_U16			Key_Timer_Function(RAW_VOID *expiration_input);

// 指向申请的内存地址
char * mem_ptr = NULL;
	
	
// 第一个任务的执行函数入口
void First_Task(void * pParam)
{		
	unsigned int count;	
	
	while(1)
	{
		// 阻塞等待按键2信号量
		raw_semaphore_get(&Key2_Semaphore, RAW_WAIT_FOREVER);
		
		// 获得系统剩余内存
		count = mem_4_free_get();
		
		Uart_Printf("free memory: %d \r\n",count);
		
	}
}

// 第二个任务的执行函数入口
void Second_Task(void * pParam)
{
	int i = 1;
	
	while(1)
	{
		// 阻塞等待按键1信号量
		raw_semaphore_get(&Key1_Semaphore,RAW_WAIT_FOREVER);
		
		// 申请内存
		mem_ptr = mem_4_malloc(1000 * i);
		
		if(NULL == mem_ptr)
		{
			Uart_Printf("memory malloc failed!\r\n");
		}
		else
		{
			i ++;
			Uart_Printf("memory malloc success, mem_ptr: %p\r\n",mem_ptr);		
		}
		
	}
}

// 第三个任务的执行函数入口
void Thrid_Task(void * pParam)
{

	while(1)
	{
		// 阻塞等待按键0信号量
		raw_semaphore_get(&Key0_Semaphore,RAW_WAIT_FOREVER);
		
		mem_4_free(mem_ptr);
		
		mem_ptr = NULL;
		
	}
}


void Heap_4_Test()
{
	
	// 创建第一个任务
	raw_task_create(&Task1, (RAW_U8  *)"task1", 0,
	                         8, 0,  Task1_Stack, 
	                         TASK_STACK_SIZE , First_Task, 1); 

	// 创建第二个任务
	raw_task_create(&Task2, (RAW_U8  *)"task2", 0,
	                         9, 0,  Task2_Stack, 
	                         TASK_STACK_SIZE , Second_Task, 1); 
	
	// 创建第三个任务
	raw_task_create(&Task3, (RAW_U8  *)"task3", 0,
	                         10, 0,  Task3_Stack, 
	                         TASK_STACK_SIZE , Thrid_Task, 1); 
	
}

