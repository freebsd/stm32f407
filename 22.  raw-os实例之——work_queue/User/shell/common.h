#ifndef _COMMON_H
#define _COMMON_H

// int型转变转字符串
void int_to_str(unsigned int value,unsigned char *buffer,unsigned char mode);

// 字符串转int型变量
void str_to_int(unsigned char *buffer,unsigned int*value);

#endif
