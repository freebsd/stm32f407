/*
           中断下半部  work_queue  模块使用
        
        本模块中的Data_Produce_simulate函数是一个模拟产生数据的函数,
        它被timer.c中的定时器中断服务函数调用,从而触发work queue执行！


*/

#include <stdio.h>
#include <lib_string.h>
#include <raw_api.h>
#include <raw_work_queue.h>

#include "usart1.h"
#include "timer.h"
#include "led.h"

#define	WORK_QUEUE_TASK_STACK_SIZE		512

PORT_STACK Work_Queue_Task_Stack[WORK_QUEUE_TASK_STACK_SIZE];
WORK_QUEUE_STRUCT Work_Queue_Task;


RAW_VOID *Work_Queue_Msg[10];
OBJECT_WORK_QUEUE_MSG work_queue_msg[100];

// 定义一个函数,它在work queue task中调用,用于数据处理
void My_Func(RAW_U32 arg, void *msg)
{
    LED_Toggle(1);
    LED_Toggle(2);
    
    // 这里打印数据来模拟数据的处理
    Uart_Printf("%d  ",arg);                    //打印传参
    Uart_Printf("%s\r\n",(char *)msg);          //打印传参
}

// 以下变量用来模拟中断中采集到的数据
unsigned char msg[] = "hello world! msg count:    ";
unsigned int  count = 0;

// 下面函数在定时器中断中被调用,模拟在中断中采集到数据,然后传送给work queue处理
void Data_Produce_simulate()
{
    /* 模拟采集到数据 */
    count ++;
    if(count > 999)count = 0;
    
    msg[sizeof(msg) - 4] = count/100 + 0X30;
    msg[sizeof(msg) - 3] = count%100/10 + 0X30;
    msg[sizeof(msg) - 2] = count%10 + 0X30;

    /* 触发work queue并传入数据count 和 msg,数据处理函数为My_Func */
    sche_work_queue(&Work_Queue_Task,count,msg,My_Func);
}

 
// raw-os的中断下半部模块Work_Queue测试
void WorkQueue_Test()
{
	// 初始化work queue system
    global_work_queue_init(work_queue_msg, 100);
	
    // 创建一个work queue
	work_queue_create(&Work_Queue_Task, 6,                              // work_queue及其优先级
                      WORK_QUEUE_TASK_STACK_SIZE, Work_Queue_Task_Stack,// work_queue_task栈 
                      (RAW_VOID **)Work_Queue_Msg, 10);                 // 指向work_queue msg的指针
    
    // 初始化硬件,在硬件中断里产生数据并触发中断下半部执行
    TIM3_Int_Init(4999,8399);                   // 500ms定时器中断
    
    
}  
