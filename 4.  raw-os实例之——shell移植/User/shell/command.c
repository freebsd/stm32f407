/*
			os version and chip id command 

*/

#include <stdio.h>
#include "raw_api.h"
#include "lib_string.h"
#include "rsh.h"
#include "usart.h"
#include "common.h"

// 定义命令结点,每个命令都需要一个结点
static xCommandLineInputListItem pxNewListItem1;		// 打印RAW-OS版本
static xCommandLineInputListItem pxNewListItem2;		// 获取CHIP ID
static xCommandLineInputListItem pxNewListItem3;		// Reboot System

// raw-os-v 命令回调: 打印RAW-OS 版本
RAW_S32 get_os_version(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, const RAW_S8 * const pcCommandString)
{
	RAW_U8 version[6] = {0};
	
	raw_strcat((char *)pcWriteBuffer,(char *)"RAW-OS Version: ");
	version[0] = RAW_OS_VERSION/1000 + 0X30;
	version[1] = '.';
	version[2] = RAW_OS_VERSION%1000/100 + 0X30;
	version[3] = RAW_OS_VERSION%100/10 + 0X30;
	version[4] = RAW_OS_VERSION%10 + 0X30;
	version[5] = '\0';
	
	raw_strcat((char *)pcWriteBuffer,(char *)version);		
	return 1;
}

// chip-id 命令回调: 打印CHIP ID号
RAW_S32 get_chip_id(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, const RAW_S8 * const pcCommandString)
{
	RAW_U32 CHIP_ID[3];
	RAW_U16 Flash_Size;
	RAW_U8  input_buffer[20]={0};
	// 读取96bits ID
	CHIP_ID[0] = *(volatile u32*)(0x1FFFF7E8);
	CHIP_ID[1] = *(volatile u32*)(0x1FFFF7EC);
	CHIP_ID[2] = *(volatile u32*)(0x1FFFF7F0);
	// 读取Flash Size大小
	Flash_Size = *(volatile u16*)(0x1FFFF7E0);
		
	raw_strcat((char *)pcWriteBuffer,(const char *)"chip id: 0X");
	int_to_str(CHIP_ID[0],input_buffer,16);
	raw_strcat((char *)pcWriteBuffer,(const char *)input_buffer);
	
	int_to_str(CHIP_ID[1],input_buffer,16);
	raw_strcat((char *)pcWriteBuffer,"  ");
	raw_strcat((char *)pcWriteBuffer,(const char *)input_buffer);

	int_to_str(CHIP_ID[2],input_buffer,16);
	raw_strcat((char *)pcWriteBuffer,(const char *)"  ");
	raw_strcat((char *)pcWriteBuffer,(const char *)input_buffer);

	raw_strcat((char *)pcWriteBuffer,(const char *)"\r\n");
	// 打印Flash Size
	raw_strcat((char *)pcWriteBuffer,(const char *)"Flash Size: ");
	int_to_str(Flash_Size,input_buffer,10);
	raw_strcat((char *)pcWriteBuffer,(const char *)input_buffer);

	raw_strcat((char *)pcWriteBuffer," KB");	
	
	return 1;
}

// reboot 命令回调: reboot system
RAW_S32 reboot(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, const RAW_S8 * const pcCommandString)
{
	NVIC_SystemReset();
	return 1;
}

// raw-os-v 命令: 命令体定义
static  const xCommandLineInput RAW_OS_Version = {
	(const RAW_S8 *)"raw-os-v",
	(const RAW_S8 *)"raw-os-v: get the raw-os version.\r\n",
	get_os_version,
	0
};

// mcu-id 命令: 命令体定义
static  const xCommandLineInput chip_id = {
	(const RAW_S8 *)"chip-id",
	(const RAW_S8 *)"chip-id: get the chip 96 bits id and flash size.\r\n",
	get_chip_id,
	0
};

// reboot 命令: 命令体定义
static  const xCommandLineInput reboot_command = {
	(const RAW_S8 *)"reboot",
	(const RAW_S8 *)"reboot: get the chip 96 bits id and flash size.\r\n",
	reboot,
	0
};

// 注册所有自定义命令
void Command_Register()
{
	// 注册命令: 获得RAW-OS版本
  rsh_register_command(&RAW_OS_Version, &pxNewListItem1);
	// 注册命令：获得96 bits chip ID,以及Flash size
	rsh_register_command(&chip_id, &pxNewListItem2);	
	
	// 注册命令：重启系统
	rsh_register_command(&reboot_command, &pxNewListItem3);	
}
