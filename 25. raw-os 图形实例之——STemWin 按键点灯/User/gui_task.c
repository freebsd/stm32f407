/*
	GUI_Task
	
*/
#include "stm32f4xx_rcc.h"
#include "raw_api.h"

#include "gui.h"
#include "TEXT.h"
#include "BUTTON.h"

#include "touch.h"
#include "usart1.h"
#include "led.h"

// 任务栈大小
#define TASK_STACK_SIZE   			256
PORT_STACK GUI_Task_Stack[TASK_STACK_SIZE];		// 任务栈
RAW_TASK_OBJ 		GUI_Task;					// 任务对像

RAW_TIMER  Touch_Timer;						    // 创建一个软件定时器,周期性的检测触摸
RAW_U16    Touch_Timer_Func(RAW_VOID *expiration_input); // 定时器的回调函数

	
// 定时器回调函数
RAW_U16 Touch_Timer_Func(RAW_VOID *expiration_input)
{	
	GUI_TOUCH_Exec();							// 执行触摸更新函数
	
	return 1;
}

// 画按键
static void Draw_Button(void) 
{	
	BUTTON_Handle hButton1;						// 定义两个按键
	BUTTON_Handle hButton2;
	GUI_SetColor(GUI_BLUE);
	GUI_SetFont(GUI_FONT_16_ASCII);				// 设置字体

	/* Create the button*/
	hButton1 = BUTTON_Create(110, 160, 100, 40, GUI_ID_OK, WM_CF_SHOW);
	hButton2 = BUTTON_Create(110, 250, 100, 40, GUI_ID_CLOSE, WM_CF_SHOW);

	BUTTON_SetText(hButton1, "LED ON");
	BUTTON_SetText(hButton2, "LED OFF");

	if(GUI_WaitKey() == GUI_ID_OK)
	{	
	  LED_ON(1);
	  LED_ON(2);
	}

	if(GUI_WaitKey() == GUI_ID_CLOSE)
	{	
	  LED_OFF(1);
	  LED_OFF(2);
	}
}

// GUI_Task任务的函数入口
void GUI_Task_Func(void * pParam)
{		
	while(1)
	{		
		Draw_Button();							// 画按钮		
		
		raw_sleep(20);
	
	}
}

// GUI初始化
void GUI_Task_Init()
{
	int xPos,yPos;
	
	/* Enable the CRC Module */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE); 
	
	if( GUI_Init() == 0)
	{
		//Uart_Printf("STemWin Init Success\r\n");
	}
	else
	{
		//Uart_Printf("STemWin Init Failed\r\n");
	}
	
	GUI_SetFont(GUI_FONT_24_ASCII);			// 设置字体
	GUI_SetBkColor(GUI_GRAY);
	GUI_SetColor(GUI_BLUE);
	GUI_Clear();
	
	xPos = LCD_GetXSize()/2;				// 得到LCD分辨率
	yPos = LCD_GetYSize()/2;
	
	GUI_DispStringHCenterAt("RAW-OS STemWin Demo",  xPos, yPos - 160);
	
	// 创建软件定时器
	// 定时周期为2个ticks即20ms
	raw_timer_create(&Touch_Timer,(RAW_U8 *)"Touch_Timer",Touch_Timer_Func,0,10,2,1);
	
	// 创建GUI Task任务
	raw_task_create(&GUI_Task,(RAW_U8 *)"gui_task",0,
	                10,0,
					GUI_Task_Stack,TASK_STACK_SIZE,
					GUI_Task_Func,1);

}


