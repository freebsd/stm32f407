/*
		XPT2046 Driver


*/

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "touch.h"


// us级延时
static void delay_xus(u32 count)
{
	u32 i;
	while(count --)
	{
		for(i = 0;i < 168; i ++);	
	}
}

 	 			    					   
// 向XPT2046写命令
void Touch_Write_Byte(u8 value)    
{  
	u8 count=0; 
	TOUCH_SCK_LOW(); 
	delay_xus(1);
	for(count = 0; count < 8; count ++)  
	{ 	  
		if(value & 0x80)		
			TOUCH_MOSI_HIGH();  
		else 
			TOUCH_MOSI_LOW(); 
		
		value <<= 1;    
		TOUCH_SCK_LOW();
		delay_xus(1);
		TOUCH_SCK_HIGH();	
		delay_xus(1);
	}		 			    
} 		 

// 读一次数据
u16 Touch_Read_AD(u8 cmd)	  
{ 	 
	u8  count = 0; 	  
	u16 Num   = 0; 
	TOUCH_CS_LOW(); 					// 使能片选	
	Touch_Write_Byte(cmd);
	delay_xus(5);
	TOUCH_SCK_LOW(); 	     	    
	delay_xus(1);    	   
	TOUCH_SCK_HIGH();		
	delay_xus(1);    
	TOUCH_SCK_LOW(); 	     	    
	for(count = 0; count < 16; count ++)
	{ 				  
		Num <<= 1; 	 
		TOUCH_SCK_LOW(); 		    	   
		delay_xus(1);    
 		TOUCH_SCK_HIGH();
		delay_xus(1); 
 		if(TOUCH_MISO())
			Num++; 		 
	}  	
	Num >>= 4;   	
	TOUCH_SCK_HIGH();		 
	return(Num);   
}

#define READ_TIMES 10 			// 采样数量	
#define LOST_VAL   4			
/**********************************************************************
* 函数名称： Touch_Read_XOY
* 功能描述： 读取X or Y方向数据
* 输入参数： cmd —— CMD_RDX 读取X方向数据
			 cmd —— CMD_RDY 读取Y方向数据
* 输出参数： 无
* 返 回 值： 去极值平均滤波后的数据
***********************************************************************/
u16 Touch_Read_XOY(u8 cmd)
{
	u16 i, j;
	u16 buf[READ_TIMES];
	u16 sum=0;
	u16 temp;
	for(i = 0; i < READ_TIMES; i ++)
		buf[i] = Touch_Read_AD(cmd);
	
	for(i = 0;i < READ_TIMES - 1; i ++)
	{
		for(j = i+1; j < READ_TIMES; j ++)
		{
			if(buf[i] > buf[j])
			{
				temp   = buf[i];
				buf[i] = buf[j];
				buf[j] = temp;
			}
		}
	}	  
	sum = 0;
	for(i = LOST_VAL; i < READ_TIMES - LOST_VAL; i++)
		sum += buf[i];
	
	temp = sum/(READ_TIMES-2*LOST_VAL);
	return temp;   
}

// 读取X方向数据
u16 Touch_Read_X()
{
	return Touch_Read_XOY(CMD_RDX);
}

// 读取Y方向数据
u16 Touch_Read_Y()
{
	return Touch_Read_XOY(CMD_RDY);
}

// 读取X和Y方向的数据
u8 Touch_Read_XY(u16 *x,u16 *y)
{
	u16 xtemp,ytemp;			 	 		  
	xtemp = Touch_Read_XOY(CMD_RDX);
	ytemp = Touch_Read_XOY(CMD_RDY);	  												   
	*x = xtemp;
	*y = ytemp;
	
	return 1;
}

#define ERR_RANGE 	50 				// 阀值
/**********************************************************************
* 函数名称： Touch_Read_XY2
* 功能描述： 读取X and Y方向数据
* 输入参数： x —— X方向数据
			 y —— Y方向数据
* 输出参数： 无
* 返 回 值： 阀值滤波
***********************************************************************/
u8 Touch_Read_XY2(u16 *x, u16 *y) 
{
	u16 x1,y1;
 	u16 x2,y2;
 	u8 flag;    
    flag = Touch_Read_XY(&x1,&y1);   
    if(flag == 0)return(0);
    flag = Touch_Read_XY(&x2,&y2);	   
    if(flag == 0)return(0);   
    if(((x2 <= x1 && x1 < x2 + ERR_RANGE) || (x1 <= x2 && x2 < x1 + ERR_RANGE)) &&
	   ((y2 <= y1 && y1 < y2 + ERR_RANGE) || (y1 <= y2 && y2 < y1 + ERR_RANGE)))
    {
        *x = (x1 + x2)/2;
        *y = (y1 + y2)/2;
        return 1;
    }
	else 
		return 0;	  
}  

//// 外部中断线1,中断服务程序
//void EXTI1_IRQHandler(void)
//{
//	
//	if(EXTI_GetITStatus(EXTI_Line1) != RESET)
//	{
//		EXTI_ClearITPendingBit(EXTI_Line1);
//	}	  
//}

/**********************************************************************
* 函数名称： Touch_Init
* 功能描述： 触摸屏接口硬件配置
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void Touch_Init()
{	
	GPIO_InitTypeDef   GPIO_InitStructure;
//	EXTI_InitTypeDef   EXTI_InitStructure;
//	NVIC_InitTypeDef   NVIC_InitStructure;
	/* 开GPIO时钟 */
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOB |
							RCC_AHB1Periph_GPIOC |
							RCC_AHB1Periph_GPIOF, ENABLE);
	/* 开系统控制块时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* 配置CS引脚 PC13  */
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_13;				
 	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP; 		 
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOC, &GPIO_InitStructure);
 	GPIO_SetBits(GPIOC,GPIO_Pin_13);
	
	/* 配置SCK引脚 PB0  */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;				
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
 	GPIO_SetBits(GPIOB,GPIO_Pin_0);//上拉
	
	/* 配置MOSI引脚 PF11 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
 	GPIO_Init(GPIOF, &GPIO_InitStructure);
 	GPIO_SetBits(GPIOF,GPIO_Pin_11);
	
	/* 配置MISO引脚 PB2 */
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_2;            
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;           					// ??
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;           					// ???? 
	GPIO_Init(GPIOB, &GPIO_InitStructure); 	
	
	/* 配置PEN引脚  PB1 */
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_1;            
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;           					
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;           					
	GPIO_Init(GPIOB, &GPIO_InitStructure);

//	/* PEN引脚中断初始化 */
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource1);

//	/* 外部中断线1,下降沿中断 */ 
//	EXTI_InitStructure.EXTI_Line 	= EXTI_Line1;                   
//	EXTI_InitStructure.EXTI_Mode 	= EXTI_Mode_Interrupt;          
//	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;      		
//	EXTI_InitStructure.EXTI_LineCmd = ENABLE;                    		
//	EXTI_Init(&EXTI_InitStructure);                              

//	/* 配置NVIC */
//	NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;             
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02; 
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);
}
