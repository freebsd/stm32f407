#ifndef _TOUCH_H
#define _TOUCH_H

#include "stm32f4xx.h"

// 硬件接口
#define TOUCH_CS_LOW()       GPIO_ResetBits(GPIOC, GPIO_Pin_13)
#define TOUCH_CS_HIGH()      GPIO_SetBits(GPIOC, GPIO_Pin_13)

#define TOUCH_MOSI_LOW()	 GPIO_ResetBits(GPIOF, GPIO_Pin_11)
#define TOUCH_MOSI_HIGH()	 GPIO_SetBits(GPIOF, GPIO_Pin_11)

#define TOUCH_MISO()	 	 GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_2)

#define TOUCH_SCK_LOW()		 GPIO_ResetBits(GPIOB, GPIO_Pin_0)
#define TOUCH_SCK_HIGH()	 GPIO_SetBits(GPIOB, GPIO_Pin_0)

#define TOUCH_PEN()	 		 GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1)

// XPT2046命令
#define  CMD_RDX  0XD0
#define  CMD_RDY  0X90


// 读X或Y方向的数据
u16 Touch_Read_XOY(u8 cmd);

// 读X和Y方向的数据
u8 Touch_Read_XY2(u16 *x, u16 *y);

// 硬件初始化
void Touch_Init(void);


u16 Touch_Read_X(void);
u16 Touch_Read_Y(void);


#endif
