/*
			raw-os 图形篇 —— STemWin 5.22 移植测试,包括触模测试
			
			STM32F4 固件库版本：V 1.3
			RAW-OS  内核版本：  V 1.056		

*/

#include <stdio.h>
#include <stdarg.h>
#include <stm32f4xx_conf.h>
#include <lib_string.h>
#include "raw_api.h"
#include "shell_task.h"

#include "GUI.h"
#include "led.h"									// LED硬件操作相关
#include "usart1.h"
#include "led_cmd.h"								// LED控制命令接口
#include "KEY.h"									// KEY硬件初始化
#include "system_cmd.h"								// 系统命令
#include "task_stack_detect_cmd.h"					// 栈检测命令
#include "lcd_cmd.h"								// LCD测试命令
#include "gui_task.h"								// stemwin初始化
#include "touch.h"									// 触摸初始化

// 启动任务栈,任务对像初始化
#define             INIT_TASK_STK_SIZE      256
PORT_STACK          Init_Task_Stack[INIT_TASK_STK_SIZE];
RAW_TASK_OBJ 		Init_Task;

// 启动任务执行函数
void Init_Task_Func(void *parg);

/**********************************************************************
* 函数名称： OS_CPU_SysTickInit
* 功能描述： RAW-OS 系统时钟初始化(STM32F407主频168MHz)
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void  OS_CPU_SysTickInit(void)
{
	/*
		配置SysTick中断步率100次/秒,故RAW-OS的系统周期10ms	
	*/
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}



/*************** 主函数 **********************/
int main()
{	
	SystemInit();									
	
	raw_os_init();								// 操作系统初始化

	// 创建启动任务,配置系统硬件,创建其它任务
	raw_task_create(&Init_Task, (RAW_U8  *)"Init_Task", 0,
	                         1, 0,  Init_Task_Stack, 
	                         INIT_TASK_STK_SIZE , Init_Task_Func, 1); 
	
	
	
	raw_os_start();								// 启动操作系统
	
	return 0;	
}

// 启动任务的执行函数
void Init_Task_Func(void *parg)
{
	// 1. Initial all the used Hardware
	
	OS_CPU_SysTickInit();						// 配置系统时钟	
	
	USART1_Init(115200);
	LED_Init();									// LED初始化
	KEY_Init();
	Touch_Init();								// 触摸接口初始化
		
	// 2. Register all the Commands
	system_command_Register();
	register_task_stack_command();
	led_command_register();
	lcd_command_register();		
	
	GUI_Task_Init();							// 创建GUI任务
	
	Shell_Task_Init();							// shell任务初始化
	
	
	raw_task_delete(raw_task_identify());		// 删除

}


