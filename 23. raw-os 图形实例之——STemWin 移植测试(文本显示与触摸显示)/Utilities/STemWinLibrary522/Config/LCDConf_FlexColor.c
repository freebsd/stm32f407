/*
	STemWin 5.22 LCD Drviver

	说明: 2.8 inch LCD 主控芯片ILI9341 测试成功			
		  3.5 inch LCD 主控芯片NT35310 测试成功
		  
    可在shell中运行lcd_test命令获得LCD驱动IC型号

*/

#include "GUI.h"
#include "GUIDRV_FlexColor.h"
#include "lcd.h"					// 添加LCD驱动头文件

// LCD 控制器选择(对应型号置1)
#define LCD_ID_9341		0			
#define LCD_ID_5310		1			
	

#if     LCD_ID_9341						// 2.8 inch

/* 配置LCD分辨率 */
#define XSIZE_PHYS  240	 				
#define YSIZE_PHYS  320 	

// 触摸校准的AD值
#define TOUCH_AD_TOP            157
#define TOUCH_AD_BOTTOM         3935
#define TOUCH_AD_LEFT           255
#define TOUCH_AD_RIGHT          3940

#elif   LCD_ID_5310						// 3.5 inch

/* 配置LCD分辨率 */
#define XSIZE_PHYS  320 				
#define YSIZE_PHYS  480 	

// 触摸校准的AD值
#define TOUCH_AD_TOP            3973
#define TOUCH_AD_BOTTOM         228
#define TOUCH_AD_LEFT           3863
#define TOUCH_AD_RIGHT          193

#else

	/* add your own lcd configuration */

#endif


/* LCD驱动器访问地址 */
#define LCD_REG		*(__IO uint16_t *)(0x6C000000)
#define LCD_RAM		*(__IO uint16_t *)(0x6C000000 | 0x00000080)


/*********************************************************************
*
*       Configuration checking
*
**********************************************************************
*/
#ifndef   VXSIZE_PHYS
  #define VXSIZE_PHYS XSIZE_PHYS
#endif
#ifndef   VYSIZE_PHYS
  #define VYSIZE_PHYS YSIZE_PHYS
#endif
#ifndef   XSIZE_PHYS
  #error Physical X size of display is not defined!
#endif
#ifndef   YSIZE_PHYS
  #error Physical Y size of display is not defined!
#endif
#ifndef   GUICC_565
  #error Color conversion not defined!
#endif
#ifndef   GUIDRV_FLEXCOLOR
  #error No display driver defined!
#endif


/********************************************************************
*       LcdWriteReg
*
* Function description:   Sets display register
  
*/
static void LcdWriteReg(U16 Data) {
	LCD_REG  = Data;
}

/********************************************************************
*
*       LcdWriteData
*
* Function description:    Writes a value to a display register
*/
static void LcdWriteData(U16 Data) {
	LCD_RAM = Data;
}

/********************************************************************
*
*       LcdWriteDataMultiple
*
* Function description:   Writes multiple values to a display register.
*/
static void LcdWriteDataMultiple(U16 * pData, int NumItems) {
  while (NumItems--) {
	  LCD_RAM = * pData++;
  }
}

/********************************************************************
*
*       LcdReadDataMultiple
*
* Function description:   Reads multiple values from a display register.
*/
static void LcdReadDataMultiple(U16 * pData, int NumItems) {
  while (NumItems--) {
	  *(pData ++) = LCD_RAM;
  }
}

/*********************************************************************
*
*       LCD_X_Config
*
* Function description:   
    Called during the initialization process in order to set up the
*   display driver configuration.
*
*/
void LCD_X_Config(void) {
	
  GUI_DEVICE * pDevice;
  CONFIG_FLEXCOLOR Config = {0};
  GUI_PORT_API PortAPI = {0};
  //
  // Set display driver and color conversion
  //
#if	LCD_ID_9341					// 9341 是BGR模式
  
  pDevice = GUI_DEVICE_CreateAndLink(GUIDRV_FLEXCOLOR, GUICC_565, 0, 0);
  
#elif	LCD_ID_5310				// 5310 是RGB模式
  
  pDevice = GUI_DEVICE_CreateAndLink(GUIDRV_FLEXCOLOR, GUICC_M565, 0, 0); 
  
#else
  
  // add your own lcd configuration
  
#endif
  
  //
  // Display driver configuration, required for Lin-driver
  //
  LCD_SetSizeEx (0, XSIZE_PHYS , YSIZE_PHYS);
  LCD_SetVSizeEx(0, VXSIZE_PHYS, VYSIZE_PHYS);
  //
  // Set controller and operation mode
  //
	
  //Config.Orientation = GUI_SWAP_XY | GUI_MIRROR_Y;
  GUIDRV_FlexColor_Config(pDevice, &Config);
                                   
  //
  // Set controller and operation mode
  //
  PortAPI.pfWrite16_A0  = LcdWriteReg;
  PortAPI.pfWrite16_A1  = LcdWriteData;
  PortAPI.pfWriteM16_A1 = LcdWriteDataMultiple;
  PortAPI.pfReadM16_A1  = LcdReadDataMultiple;
  GUIDRV_FlexColor_SetFunc(pDevice, &PortAPI, GUIDRV_FLEXCOLOR_F66709, GUIDRV_FLEXCOLOR_M16C0B16);
  
  // 校准
  GUI_TOUCH_Calibrate(GUI_COORD_X, 0, XSIZE_PHYS - 1, TOUCH_AD_LEFT, TOUCH_AD_RIGHT);
  GUI_TOUCH_Calibrate(GUI_COORD_Y, 0, YSIZE_PHYS - 1, TOUCH_AD_TOP, TOUCH_AD_BOTTOM);
  
}

/*********************************************************************
*
*       LCD_X_DisplayDriver
*
* Function description:
*   This function is called by the display driver for several purposes.
*   To support the according task the routine needs to be adapted to
*   the display controller. Please note that the commands marked with
*   'optional' are not cogently required and should only be adapted if
*   the display controller supports these features.
*
* Parameter:
*   LayerIndex - Index of layer to be configured
*   Cmd        - Please refer to the details in the switch statement below
*   pData      - Pointer to a LCD_X_DATA structure
*
* Return Value:
*   < -1 - Error
*     -1 - Command not handled
*      0 - Ok
*/
int LCD_X_DisplayDriver(unsigned LayerIndex, unsigned Cmd, void * pData) {
  int r;
  (void) LayerIndex;
  (void) pData;
  
  switch (Cmd) {
  case LCD_X_INITCONTROLLER: {
    //
    // Called during the initialization process in order to set up the
    // display controller and put it into operation. If the display
    // controller is not initialized by any external routine this needs
    // to be adapted by the customer...
    //
    LCD_Init();
    return 0;
  }
  default:
    r = -1;
  }
  return r;
}

/*************************** End of file ****************************/

