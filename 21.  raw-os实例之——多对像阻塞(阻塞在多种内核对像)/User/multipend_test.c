/*
            多对像阻塞 测试
            
       创建一个任务同时阻塞在一个消息队列和一个信号量上，任何一个队列中有消息都会唤醒任务去读取并处理。
       
       按raw-os多对像阻塞的理论,这里建立一个"网关",即信号量,其它的内核对像在有效时会通知这个网关(释放信号量),
       
       从而唤醒阻塞的任务。
        


*/

#include <stdio.h>
#include <lib_string.h>
#include "raw_api.h"
#include "usart1.h"
#include "led.h"

// 阻塞在多对像上的任务资源
#define TASK_STACK_SIZE   			128				// 消息处理任务栈大小
PORT_STACK    Task_MultiPend_Stack[TASK_STACK_SIZE];// 消息处理任务栈
RAW_TASK_OBJ  Task_MultiPend;						// 消息处理任务对像


RAW_QUEUE MultiPend_Queue;              // 定义一个队列
RAW_VOID *multipend_queue[10];          // Queue存储消息的指针数组

RAW_SEMAPHORE MultiPend_Sem;            // 定义一个信号量

RAW_SEMAPHORE GateWay_Sem;              // 用作通知任务处理的网关

// 队列1异步通知回调函数
void notify_queue(RAW_QUEUE *ptr)
{
	raw_semaphore_put(&GateWay_Sem);    // 通知网关
}

// 队列2异步通知回调函数
void notify_sem(RAW_SEMAPHORE *ptr)
{	
	raw_semaphore_put(&GateWay_Sem);    // 通知网关
}


// 任务执行函数
void Task_MultiPend_Fun(void * pParam)
{
	RAW_U16 res;
	signed char *msg;
		
	while(1)
	{
		// 等待消息(永久)        
        raw_semaphore_get (&GateWay_Sem, RAW_WAIT_FOREVER); 

        /* 判断是哪一个对像 */        
        // 如果MultiPend_Queue中有消息则读取,如果 无,则立即返回
        res = raw_queue_receive(&MultiPend_Queue, RAW_NO_WAIT, (RAW_VOID **)&msg);
        
        if (res == RAW_SUCCESS)             // MultiPend_Queue中得到消息
        {   
            // 取得消息        
            USART1_Send_String(msg);		// 打印消息
            USART1_Send_String((RAW_S8 *)"\r\n");
            LED_Toggle(1);
        }
        else                                // MultiPend_Sem中得到消息
        {
            res = raw_semaphore_get (&MultiPend_Sem, RAW_NO_WAIT);
            if (res == RAW_SUCCESS)       
            {        
                LED_Toggle(2);      
            }
        }
    }
}


// RAW_QUEUE测试,创建一个任务阻塞在队列上,如果5S内没有消息,则超时返回
void MultiPend_Test()
{
	// 1. 创建一个队列和一个信号量,任务阻塞在这两个内核对像上
    raw_queue_create(&MultiPend_Queue, (RAW_U8 *)"MultiPend_Queue", (RAW_VOID **)&multipend_queue, 10);
    raw_semaphore_create(&MultiPend_Sem, (RAW_U8 *)"MultiPend_Sem", 0);
    
    // 2. 创建一个信号量用作通知任务的网关
    raw_semaphore_create(&GateWay_Sem, (RAW_U8 *)"GateWay_Sem", 0);

    // 2. 注册内核对像对像对应的回调函数
	raw_queue_send_notify(&MultiPend_Queue, notify_queue);
	raw_semphore_send_notify(&MultiPend_Sem, notify_sem);   

    
    // 3. 创建消息处理任务
	raw_task_create(&Task_MultiPend, (RAW_U8  *)"Task_Multi", 0,
	                10, 0,  Task_MultiPend_Stack, 
	                TASK_STACK_SIZE , Task_MultiPend_Fun, 1); 
}  
