/*
            Queue 测试
            
       创建一个任务阻塞在消息队列上,按键按下发送消息,并根据不同的消息内容控制不同的LED亮灭
        


*/

#include <stdio.h>
#include <lib_string.h>
#include "raw_api.h"
#include "usart1.h"
#include "led.h"

#define TASK_STACK_SIZE   			128							// 消息处理任务栈大小
PORT_STACK    Task_Queue_Stack[TASK_STACK_SIZE];				// 消息处理任务栈
RAW_TASK_OBJ  Task_Queue;										// 消息处理任务

extern RAW_QUEUE  Key_Queue;									// 在key.c文件中定义的消息队列


// 消息处理任务执行函数
void Task_Queue_Fun(void * pParam)
{
	RAW_U16 res;
	RAW_U32 timeout_value = 500;				// 500 个时钟周期,即5S
	signed char *msg;
		
	while(1)
	{
		// 等待消息(等待5秒钟,如果还没有消息,则超时返回)
		res = raw_queue_receive(&Key_Queue, timeout_value, (RAW_VOID **)&msg);	
        
		if( RAW_BLOCK_TIMEOUT == res )							// 超时返回
		{
			USART1_Send_String((RAW_S8 *)"\r\n");	
			USART1_Send_String((RAW_S8 *)"receive queue timeout!\r\n");
		}
		else if( RAW_SUCCESS == res)							// 队列中有消息
		{
			USART1_Send_String(msg);			// 打印消息
			USART1_Send_String((RAW_S8 *)"\r\n");
		  
			// 根据消息内容点灯
			if( 0 == raw_strncmp((const char *)msg,"KEY0",4) )	// 消息内容为KEY0						
			{
				LED_Toggle(1);
			}
			else if(0 == raw_strncmp((const char *)msg,"KEY1",4) )// 消息内容为KEY1
			{
				LED_Toggle(2);
			}
			else if(0 == raw_strncmp((const char *)msg,"KEY2",4) )// 消息内容为KEY2
			{
				LED_Toggle(1);
				LED_Toggle(2);
			}
		}	

	}
}


// RAW_QUEUE测试,创建一个任务阻塞在队列上,如果5S内没有消息,则超时返回
void Queue_Test()
{
	// 创建消息处理任务
	raw_task_create(&Task_Queue, (RAW_U8  *)"Task_Queue", 0,
	                10, 0,  Task_Queue_Stack, 
	                TASK_STACK_SIZE , Task_Queue_Fun, 1); 
}   
    
 
