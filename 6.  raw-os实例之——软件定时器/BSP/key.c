/*
		KEY 驱动函数
			
		说明: 在周期定时器回调函数中读取按键是否按下,
		      作了标志处理,可以很稳定的检测按键是否按下		     
		

*/

#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "raw_api.h"
#include "USART1.h"


RAW_SEMAPHORE	Key_Semaphore;		// 定义一个信号量,用于按键与任务同步

RAW_TIMER       Key_Timer;			// 定义一个软件定时器,用于周期性检测按键

// 周期软件定时器回调函数
RAW_U16 Key_Timer_Function(RAW_VOID *expiration_input);

// 定义按键的三种状态
typedef enum{
    State_UP = 0,           // 松开状态
    State_Press,            // 按下状态
    State_Press_OK          // 等待松开状态
}KEY_STATE;

KEY_STATE Key_State = State_UP;

/**********************************************************************
* 函数名称： KEY_Init
* 功能描述： KEY GPIO 配置
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void KEY_Init()
{
	GPIO_InitTypeDef   GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,  ENABLE); 					// 使能GPIOA时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,  ENABLE); 					// 使能GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE); 					// 使能SYSCFG时钟

	// PA0对应KEY_UP键
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;						    // 浮空输入  
	GPIO_Init(GPIOA, &GPIO_InitStructure);  

	// PE2,3,4分别接KEY2,KEY1,KEY0
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4;            
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;           					// 输入
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;           					// 设置上拉 
	GPIO_Init(GPIOE, &GPIO_InitStructure);  

	// 创建信号量,用于按键与任务同步
	raw_semaphore_create(&Key_Semaphore,(RAW_U8 *)"KEY_SEMAPHORE",0);
    
    // 创建周期性定时器
    // 定时器在创建后100个ticks即1S后开始运行自动运行
    // 定时器的执行周期为2个ticks,即20ms
	raw_timer_create(&Key_Timer,(RAW_U8 *)"Key_Timer",Key_Timer_Function,0,100,2,1);
}

/**********************************************************************
* 函数名称： Key_Timer_Function
* 功能描述： 周期软件定时器回调函数(检测按键)
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
RAW_U16 Key_Timer_Function(RAW_VOID *expiration_input)
{
   
    switch(Key_State)                        // 判断按键状态
    {
        case State_UP:                       // 按键处于松开状态
        {
            // 按键0,1,2被按下
            if( !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_2) ||	
			    !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_3) ||
			    !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_4) )
            {
                Key_State = State_Press;     // 转到按下状态                                                                  
            }  
        }
            
        break;        
        
        case State_Press:                    // 按键处于按下状态
        {
            if(!GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_2) )
            {			
                // 释放信号量(唤醒阻塞在该信号量上的最高优先级任务)	
                raw_semaphore_put(&Key_Semaphore);	
                Key_State = State_Press_OK;  // 转到按下OK状态(等待松开)  
            }
            else if( !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_3) )
            {
                // 释放信号量(唤醒阻塞在该信号量上的最高优先级任务)	
                raw_semaphore_put(&Key_Semaphore);	
                Key_State = State_Press_OK;  // 转到按下OK状态(等待松开) 
                
            }
            else if( !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_4))
            {
                // 释放信号量(唤醒所有阻塞在该信号量上的任务)	
                raw_semaphore_put_all(&Key_Semaphore);
                Key_State = State_Press_OK; // 转到按下OK状态(等待松开) 
            } 
            else
            {
                Key_State = State_UP;       // 按下无效
            }
        }
        break;
        
        case State_Press_OK:                // 按下等待松开
        {
            // 按键释放
            if( GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_2) &&		
                GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_3) &&
                GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_4) )
            {
                Key_State = State_UP;	   // 松开状态									
            }         
        }
        break;  
    }
    
	return 0;
}


