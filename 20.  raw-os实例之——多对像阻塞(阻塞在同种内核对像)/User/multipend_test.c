/*
            多对像阻塞 测试
            
       创建一个任务阻塞在两个消息队列上，任何一个队列中有消息都会唤醒任务去读取并处理。
       
       按raw-os多对像阻塞的理论,这里建立一个"网关",即信号量,当消息队列中有消息时会通知这个网关(释放信号量),
       
       从而唤醒阻塞的任务。
        


*/

#include <stdio.h>
#include <lib_string.h>
#include "raw_api.h"
#include "usart1.h"
#include "led.h"

// 阻塞在多对像上的任务资源
#define TASK_STACK_SIZE   			128				// 消息处理任务栈大小
PORT_STACK    Task_MultiPend_Stack[TASK_STACK_SIZE];// 消息处理任务栈
RAW_TASK_OBJ  Task_MultiPend;						// 消息处理任务对像


RAW_QUEUE Queue_1, Queue_2, Queue_3;     // 定义三个队列

RAW_VOID *multi_queue1[10];     // Queue_1存储消息的指针数组
RAW_VOID *multi_queue2[10];     // Queue_2存储消息的指针数组
RAW_VOID *multi_queue3[10];     // Queue_3存储消息的指针数组

RAW_SEMAPHORE GateWay_Sem;    // 用作通知任务处理的网关

// 队列1回调函数
void notify_queue_1(RAW_QUEUE *ptr)
{
	raw_semaphore_put(&GateWay_Sem);
}

// 队列2回调函数
void notify_queue_2(RAW_QUEUE *ptr)
{	
	raw_semaphore_put(&GateWay_Sem);
}

// 队列3回调函数
void notify_queue_3(RAW_QUEUE *ptr)
{	
	raw_semaphore_put(&GateWay_Sem);
}



// 任务执行函数
void Task_MultiPend_Fun(void * pParam)
{
	RAW_U16 res;
	signed char *msg;
		
	while(1)
	{
		// 等待消息(永久)        
        raw_semaphore_get(&GateWay_Sem, RAW_WAIT_FOREVER); 

        /* 判断是哪一个队列中有消息 */
        
        // 如果Queue_1中有消息则读取,如果 无,则立即返回
        res = raw_queue_receive(&Queue_1, RAW_NO_WAIT, (RAW_VOID **)&msg);
        
        if (res == RAW_SUCCESS)             // Queue_1中得到消息
        {   
            
        }
        else                                // Queue_2或Queue_3中得到消息
        {
            res = raw_queue_receive(&Queue_2, RAW_NO_WAIT, (RAW_VOID **)&msg);
            
            if (res == RAW_SUCCESS)         // Queue_2中得到消息
            {          
                
            }
            else                            // Queue_3中得到消息
            {
                res = raw_queue_receive(&Queue_3, RAW_NO_WAIT, (RAW_VOID **)&msg);
            }
        }
                  
        USART1_Send_String(msg);			// 打印消息
        USART1_Send_String((RAW_S8 *)"\r\n");
      
        // 根据消息内容点灯
        if( 0 == raw_strncmp((const char *)msg,"KEY0",4) )	// 消息内容为KEY0						
        {
            LED_Toggle(1);
        }
        else if(0 == raw_strncmp((const char *)msg,"KEY1",4) )// 消息内容为KEY1
        {
            LED_Toggle(2);
        }
        else if(0 == raw_strncmp((const char *)msg,"KEY2",4) )// 消息内容为KEY2
        {
            LED_Toggle(1);
            LED_Toggle(2);
        }
    }
}


// RAW_QUEUE测试,创建一个任务阻塞在三个队列上
void MultiPend_Test()
{
	// 1. 创建三个队列,任务阻塞在这三个队列上
    raw_queue_create(&Queue_1, (RAW_U8 *)"queue1", (RAW_VOID **)&multi_queue1, 10);
	raw_queue_create(&Queue_2, (RAW_U8 *)"queue2", (RAW_VOID **)&multi_queue2, 10);
    raw_queue_create(&Queue_3, (RAW_U8 *)"queue3", (RAW_VOID **)&multi_queue3, 10);
    
    // 2. 创建一个信号量用作通知任务的网关
    raw_semaphore_create(&GateWay_Sem, (RAW_U8 *)"GateWay_Sem", 0);

    // 3. 注册队列的回调函数,当队列中有数据时会调用该回调函数
	raw_queue_send_notify(&Queue_1, notify_queue_1);
	raw_queue_send_notify(&Queue_2, notify_queue_2);   
    raw_queue_send_notify(&Queue_3, notify_queue_3);     
    
    
    // 3. 创建消息处理任务
	raw_task_create(&Task_MultiPend, (RAW_U8  *)"Task_Multi", 0,
	                10, 0,  Task_MultiPend_Stack, 
	                TASK_STACK_SIZE , Task_MultiPend_Fun, 1); 
}  
