/*
            任务创建演示程序



*/

#include "raw_api.h"
#include "led.h"

// 任务栈大小
#define TASK_STACK_SIZE   			64

// 任务栈
PORT_STACK Task1_Stack[TASK_STACK_SIZE];
PORT_STACK Task2_Stack[TASK_STACK_SIZE];

// 任务对像
RAW_TASK_OBJ 		Task1;
RAW_TASK_OBJ 		Task2;


// 第一个任务的函数入口
void First_Task(void * pParam)
{
	while(1)
	{
		LED_Toggle(1);      // 翻转LED1
		raw_sleep(10);		// 休眠100ms
	}
}


// 第二个任务的函数入口
void Second_Task(void * pParam)
{
	while(1)
	{
		LED_Toggle(2);      // 翻转LED2
		raw_sleep(50);	    // 休眠500ms
	}
}

// 创建两个任务用于演示
void Task_Test()
{
    
    // 创建第一个任务,优先级7,立即执行
	raw_task_create(&Task1, (RAW_U8  *)"task1", 0,
	                         7, 0,  Task1_Stack, 
	                         TASK_STACK_SIZE , First_Task, 1); 

	// 创建第二个任务,优先级8,立即执行
	raw_task_create(&Task2, (RAW_U8  *)"task2", 0,
	                         8, 0,  Task2_Stack, 
	                         TASK_STACK_SIZE , Second_Task, 1); 
}

