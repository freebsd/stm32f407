/*
            Queue_Buffer 测试
            
       创建一个任务阻塞在消息队列上,按键按下发送消息及长度,接收时同时接收消息及长度
        


*/

#include <stdio.h>
#include <lib_string.h>
#include "raw_api.h"
#include "usart1.h"
#include "led.h"

#define TASK_STACK_SIZE   			128							// 消息处理任务栈大小
PORT_STACK    Task_Queue_Buffer_Stack[TASK_STACK_SIZE];			// 消息处理任务栈
RAW_TASK_OBJ  Task_Queue;										// 消息处理任务

extern RAW_QUEUE_BUFFER  Key_Queue;								// 在key.c文件中定义的消息队列


// 消息处理任务执行函数
void Task_Queue_Buffer_Fun(void * pParam)
{
	RAW_U16 res;
	RAW_U32 timeout_value = 500;				// 500 个时钟周期,即5S
	RAW_U8 msg[128];                            // 消息缓冲
    MSG_SIZE_TYPE  msg_length;                  // 消息大小

    // 接收缓冲清空
	raw_memset(msg, 0, sizeof(msg));
    
	while(1)
	{
		// 等待消息(等待5秒钟,如果还没有消息,则超时返回)
		res = raw_queue_buffer_receive(&Key_Queue, timeout_value, msg, &msg_length);	
        
		if( RAW_BLOCK_TIMEOUT == res )							// 超时返回
		{
			Uart_Printf("receive queue timeout!\r\n");
		}
		else if( RAW_SUCCESS == res)							// 队列中有消息
		{
			// 打印消息及其长度
            Uart_Printf("%s, queue_size: %d\r\n",msg,msg_length);
         
			// 根据消息内容点灯
			if( 0 == raw_strncmp((const char *)msg,"KEY0",4) )	// 消息内容为KEY0						
			{
				LED_Toggle(1);
			}
			else if(0 == raw_strncmp((const char *)msg,"KEY1",4) )// 消息内容为KEY1
			{
				LED_Toggle(2);
			}
			else if(0 == raw_strncmp((const char *)msg,"KEY2",4) )// 消息内容为KEY2
			{
				LED_Toggle(1);
				LED_Toggle(2);
			}
		}
	}
}


// RAW_QUEUE_BUFFER测试,创建一个任务阻塞在队列上,如果5S内没有消息,则超时返回
void Queue_Buffer_Test()
{
	// 创建消息处理任务
	raw_task_create(&Task_Queue, (RAW_U8  *)"Task_Queue_Buffer", 0,
	                10, 0,  Task_Queue_Buffer_Stack, 
	                TASK_STACK_SIZE , Task_Queue_Buffer_Fun, 1); 
}   
    
 
