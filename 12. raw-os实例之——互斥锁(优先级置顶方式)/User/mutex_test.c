/*
        用Mutex保护共享资源 演示
        
      注：mutex解决优先级反转的方式为优先级置顶


*/

#include "raw_api.h"
#include "shell_task.h"
#include "USART1.h"
#include "lcd.h"

// 任务栈大小
#define TASK_STACK_SIZE   			256

// 任务栈
PORT_STACK Task1_Stack[TASK_STACK_SIZE];
PORT_STACK Task2_Stack[TASK_STACK_SIZE];

// 任务对像
RAW_TASK_OBJ 		Task1;
RAW_TASK_OBJ 		Task2;

// 定义一个mutex,用于保护临界资源(这里具体是保护LCD)
RAW_MUTEX  lcd_mutex;

// 延时函数
static void delay_ms(int count)
{
	unsigned int i,j;
	while(count --)
	{
		for(i = 0; i < 1000; i ++)
		{
			for(j = 0; j < 168; j ++);		
		}
	}
}
// 第一个任务的执行函数入口
void First_Task(void * pParam)
{		
	while(1)
	{
		// 1. 任务1优先级高,但要使用临界资源,也要先获得mutex
		raw_mutex_get(&lcd_mutex,RAW_WAIT_FOREVER);
		// 在占用mutex期间,其优先级被提高到创建mutex时指定的优先级8
		Uart_Printf("Task1 get mutex,Task1 current Priority is: %d\r\n",Task1.priority);		
		
		// 2. 获得成功后,使用临界资源
		LCD_Clear(LCD_COLOR_MAGENTA);
		LCD_ShowString(20,180,200,16,16,(u8 *)"Hello This is First Task");
		delay_ms(500);				/* 模拟使用一段时间 */
		
		// 3. 使用完毕,释放临界资源
		raw_mutex_put(&lcd_mutex);
		// 释放mutex后,其优先级恢复为10
		Uart_Printf("Task1 put mutex,Task1 current Priority is: %d\r\n",Task1.priority);
		
		raw_sleep(10);
	}
}

// 第二个任务的执行函数入口
void Second_Task(void * pParam)
{
	char str[] = "This is Second Task: ";
	int  i = 0;
	
	while(1)
	{
		// 1. 任务2优先级低,要使用临界资源,需等待Task1释放
		raw_mutex_get(&lcd_mutex,RAW_WAIT_FOREVER);	
			
		// 在占用mutex期间,其优先级被提高到创建mutex时指定置顶的优先级8
		Uart_Printf("Task2 get mutex, Task2 current Priority is: %d\r\n",Task2.priority);
		
		// 2. 获得mutex成功后,使用临界资源
		i = 0;		
		while(++i <= 10)	
		{			
			delay_ms(200);								/* 模拟使用一段时间 */	
			LCD_Clear(LCD_COLOR_GREY);
			str[sizeof(str) - 2] = i + 0x30; 			// LCD上打印执行次数
			LCD_ShowString(20,200,200,16,16,(u8 *)str);
		}
		
		// 3. 使用完毕后,释放临界资源,此时会唤醒阻塞在该mutex上的Task1
		raw_mutex_put(&lcd_mutex);
		// 释放mutex后,其优先级恢复为11
		Uart_Printf("Task2 put mutex,Task2 current Priority is: %d\r\n",Task2.priority);

		raw_sleep(10);
		
	}
}


void Mutex_Test()
{
	// 创建第一个任务
	raw_task_create(&Task1, (RAW_U8  *)"task1", 0,
	                         10, 0,  Task1_Stack, 
	                         TASK_STACK_SIZE , First_Task, 1); 

	// 创建第二个任务
	raw_task_create(&Task2, (RAW_U8  *)"task2", 0,
	                         11, 0,  Task2_Stack, 
	                         TASK_STACK_SIZE , Second_Task, 1); 
    
    	
	// 创建一个互斥锁,用于保护LCD临界资源(解决优先级反转的方式为优先级置顶方法)
	// 任务占有mutex时置顶的优先级为8
	raw_mutex_create(&lcd_mutex, (RAW_U8 *)"lcd_mutex", RAW_MUTEX_CEILING_POLICY  , 8);


}
