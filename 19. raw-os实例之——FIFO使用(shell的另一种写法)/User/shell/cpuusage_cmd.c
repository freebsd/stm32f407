/*
		CPU利用率统计命令
        
        shell中输入cpuusage,打印CPU实时利用率和最大利用率
			
		
*/
#include "stdio.h"
#include "raw_api.h"
#include "rsh.h"
#include "lib_string.h"
#include "usart1.h"


// 定义命令结点
static xCommandLineInputListItem pxNewListItem1;		


// Shell 中cpuusage命令回调函数
RAW_S32 cpu_usage_test(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, RAW_S8 * pcCommandString)
{    
    
    // 打印CPU利用率和最大CPU利用率
    Uart_Printf("CPU Usage: %d %%   CPU Max Usage: %d %%\r\n",cpu_usuage,cpu_usuage_max);    
    
    return 1;
}

// 命令体定义
static  const xCommandLineInput cpuusage_command = {
	(const RAW_S8 *)"cpuusage",
	(const RAW_S8 *)"cpuusage:     the cpu usage statistic.",
	(pdCOMMAND_LINE_CALLBACK)cpu_usage_test,
	0
};

// 注册命令
void cpuusage_command_register()
{
	// 注册控制硬件的命令
	rsh_register_command(&cpuusage_command, &pxNewListItem1);

}

