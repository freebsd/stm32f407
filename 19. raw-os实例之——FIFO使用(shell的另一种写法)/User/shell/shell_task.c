/*
		raw os shell task 
		
		note: task priority is only higher than idle task and stat task

*/
#include "raw_api.h"
#include "rsh.h"
#include "lib_string.h"
#include <fifo.h>                           // RAW-OS FIFO模块

#include "USART1.h"							// 底层UART驱动


// USART1.c中定义
extern RAW_SEMAPHORE USART1_Receive_Semaphore;
extern struct raw_fifo USART1_FIFO;

// 重定向USART Input/Output Function 
#define USART_Send_Char		USART1_Send_Char
#define USART_Send_String	USART1_Send_String
#define USART_Receive_Char  USART1_Receive_Char

// define Shell Task stack size 
#define SHELL_TASK_STACK_SIZE			256

// define shell task stack and task obj 
PORT_STACK Shell_Task_Stack[SHELL_TASK_STACK_SIZE];
RAW_TASK_OBJ Shell_Task;


// input buffer and output buffer 
static RAW_S8 input_buffer[256];
static RAW_S8 output_buffer[1024];

// print a enter key
void USART_Send_Enter()
{
	USART_Send_Char('\r');
	USART_Send_Char('\n');
}

// Shell Task Execute Function
void Shell_Task_Func(void *parg)
{
	RAW_U8 ret;
    RAW_U32 len;
	RAW_U8 *raw_os = (RAW_U8 *)"raw-os#";	
	
	while(1)
	{
		USART_Send_String(raw_os);			// 打印raw-os#

		/* 等待信号量(永远阻塞,直到终端输入回册符后唤醒) */ 
        raw_semaphore_get(&USART1_Receive_Semaphore,RAW_WAIT_FOREVER);
		
		// print a enter 
		USART_Send_Enter();
		
        // 从FIFO中读出命令行
        len = fifo_out_all(&USART1_FIFO, (void *)input_buffer);
		if( len )
		{
	        input_buffer[len] = '\0';
            
            // 清空输出缓冲区
			raw_memset(output_buffer,0,1024);
			
			do
			{				
				// 命令处理
				ret = rsh_process_command((const RAW_S8 *)input_buffer,output_buffer,1024);
				
				// 打印处理结果
				USART_Send_String((RAW_U8 *)output_buffer);
				
				USART_Send_Enter(); 	// 回册换行
				
			}while(ret == 0);
		}		
	}
}

/**********************************************************************
* 函数名称： Shell_Task_Init
* 功能描述： 初始化Shell任务,并注册所有的shell命令
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void Shell_Task_Init()
{
	
	// Shell Task Create
	raw_task_create(&Shell_Task,(RAW_U8 *)"Shell Task",0,
					CONFIG_RAW_PRIO_MAX-3,0,		// 优先级仅高于空闲和统计任务
					Shell_Task_Stack,SHELL_TASK_STACK_SIZE,
					Shell_Task_Func,1);		
	
}


