/*
		Some common function used in string and figure	

*/


/**********************************************************************
* 函数名称： int_to_str
* 功能描述： int型变量转字符串
* 输入参数： value  —— 整形数  
						 mode   —— 转换进制
* 输出参数： buffer —— 转换后的结果缓存
* 返 回 值： 无
***********************************************************************/
unsigned char HEX[16] = "0123456789ABCDEF";
void int_to_str(unsigned int value,unsigned char *buffer,unsigned char mode)
{	
	unsigned char i = 0,j = 0;
	unsigned char temp_value;
	while(value)										// value > 0
	{
		buffer[i++] = HEX[value%mode];
		value = value/mode;						// value
	}
	buffer[i] = '\0';	
	
	if(i == 0)buffer[0] = '\0';
	
	for(j=0;j<i/2;j++)
	{
		temp_value = buffer[j];
		buffer[j] = buffer[i-j-1];
		buffer[i-j-1] = temp_value;
	}	
}


/**********************************************************************
* 函数名称： str_to_int
* 功能描述： int型变量转字符串
* 输入参数： buffer —— 转换前的字符串
						 
* 输出参数： value  —— 转换后的整形数 
* 返 回 值： 无
***********************************************************************/
void str_to_int(unsigned char *buffer,unsigned int*value)
{	
	unsigned int temp = 0;
	while(*buffer != '\0')
	{
		temp = temp * 10 + (*buffer++) - '0';
	}
	
	*value = temp;
}
