/*
			KEY 驱动函数
			
*/

#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "raw_api.h"
#include "USART1.h"

// 定义一个软件定时器,用于周期性检测按键
RAW_TIMER     Key_Timer;

// 周期软件定时器回调函数
RAW_U16 Key_Timer_Function(RAW_VOID *expiration_input);


/**********************************************************************
* 函数名称： KEY_Init
* 功能描述： KEY GPIO 配置
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void KEY_Init()
{
  GPIO_InitTypeDef   GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,  ENABLE); 					// 使能GPIOA时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,  ENABLE); 					// 使能GPIOA时钟
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE); 					// 使能SYSCFG时钟

	// PA0对应KEY_UP键
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;								// 浮空输入  
  GPIO_Init(GPIOA, &GPIO_InitStructure);  
	
	// PE2,3,4分别接KEY2,KEY1,KEY0
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4;            
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;           					// 输入
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;           					// 设置上拉 
  GPIO_Init(GPIOE, &GPIO_InitStructure); 	
	
}

/**********************************************************************
* 函数名称： Key_Timer_Function
* 功能描述： 周期软件定时器回调函数(检测按键)
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
RAW_U16 Key_Timer_Function(RAW_VOID *expiration_input)
{
		static int Key_Flag		 = 0;
	
		if(Key_Flag)																				// 检测按键
		{					
				if( !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_2) )
				{
				}
				
				if( !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_3) )
				{
				}
				
				if( !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_4))
				{
					
				}	
				
				Key_Flag = 0;
		}
		
		if(Key_Flag == 0)
		{
				if( !GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_2) ||		// 按键0,1,2被按下
						!GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_3) ||
						!GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_4) )
				{
							Key_Flag = 1;																	// 置标志位,在下次中断中再读取	
				}
		}
		
		return 0;
}


