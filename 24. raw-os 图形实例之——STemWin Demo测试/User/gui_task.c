/*
	GUI_Task
	
	说明: 创建两个任务:     1. LED闪烁
						    2. STemWin 官方 GUI Demo
          创建一个定时器:   定时器里检查触摸
	
*/
#include "stm32f4xx_rcc.h"
#include "raw_api.h"

#include "gui.h"

#include "touch.h"
#include "usart1.h"
#include "led.h"	
#include "guidemo.h"									 // STemWin Demo

// 任务栈大小
#define TASK_STACK_SIZE   			256
PORT_STACK GUI_Task_Stack[TASK_STACK_SIZE];				 // 任务栈
RAW_TASK_OBJ 		GUI_Task;							 // 任务对像

#define TASK_DEMO_STACK_SIZE   		2048
PORT_STACK GUI_Demo_Stack[TASK_DEMO_STACK_SIZE];		 // 任务栈
RAW_TASK_OBJ 		GUI_Demo_Task;						 // 任务对像

RAW_TIMER  Touch_Timer;						    		 // 创建一个软件定时器,周期性的检测触摸
RAW_U16    Touch_Timer_Func(RAW_VOID *expiration_input); // 定时器的回调函数

volatile char Touch_Flag = 0;
	
// 定时器回调函数
RAW_U16 Touch_Timer_Func(RAW_VOID *expiration_input)
{	 

	GUI_TOUCH_Exec();		// 执行触摸更新函数
	
	return 1;
}


// GUI_Task任务的函数入口(LED任务)
void GUI_Task_Func(void * pParam)
{		
	while(1)
	{
		LED_Toggle(1);
		raw_sleep(20);
	}
}

// STemWin GUI Demo
void GUI_Demo_Task_Func(void * pParam)
{		
	
	while(1)
	{	
		GUIDEMO_Main();
		raw_sleep(10);
	}
}

// GUI初始化
void GUI_Task_Init()
{
	int xPos,yPos;
	
	/* Enable the CRC Module */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE); 
	
	if( GUI_Init() == 0)
	{
		//Uart_Printf("STemWin Init Success\r\n");
	}
	else
	{
		//Uart_Printf("STemWin Init Failed\r\n");
	}
	
	GUI_SetFont(GUI_FONT_32_ASCII);			// 设置字体
	GUI_SetBkColor(GUI_BLUE);				// 设置背景色
	GUI_Clear();
	
	xPos = LCD_GetXSize()/2;				// 得到LCD分辨率
	yPos = LCD_GetYSize()/2;
	
	GUI_DispStringHCenterAt("Hello RAW-OS !",  xPos,yPos-40);
	GUI_DispStringHCenterAt("Hello STemWin !", xPos,yPos);
	
	// 创建软件定时器
	// 定时周期为2个ticks即20ms
	raw_timer_create(&Touch_Timer,(RAW_U8 *)"Touch_Timer",Touch_Timer_Func,0,10,2,1);
	
	// 创建GUI Task任务,LED闪烁
	raw_task_create(&GUI_Task,(RAW_U8 *)"gui_task",0,
	                10,0,
					GUI_Task_Stack,TASK_STACK_SIZE,
					GUI_Task_Func,1);
	
	
	// 创建GUI Task任务
	raw_task_create(&GUI_Demo_Task,(RAW_U8 *)"gui_demo_task",0,
	                9,0,
					GUI_Demo_Stack,TASK_DEMO_STACK_SIZE,
					GUI_Demo_Task_Func,1);
}


