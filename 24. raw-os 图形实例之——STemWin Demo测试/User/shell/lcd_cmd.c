/*
		LCD Commands 
		lcd_test: lcd display test

*/
#include "stdio.h"
#include "raw_api.h"
#include "lib_string.h"
#include "rsh.h"						// Shell 
#include "lcd.h"					

// ������
static xCommandLineInputListItem pxNewListItem1;		

// �ڲ���ʱ����
static void delay(int count)
{
	unsigned int i;
	while(count --)
	{
		for(i = 0; i < 10000; i ++);
	}
}

// lcd_test����ִ�к���
RAW_S32 lcd_test(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, RAW_S8 * pcCommandString)
{	
	LCD_Init();
	delay(1000);
	LCD_Clear(LCD_COLOR_YELLOW);
	delay(1000);
	LCD_Clear(LCD_COLOR_RED);
	delay(1000);
	LCD_Clear(LCD_COLOR_BLUE);
	delay(1000);
	LCD_Clear(LCD_COLOR_GREEN);
	delay(1000);
	LCD_Clear(LCD_COLOR_BLACK);
	delay(1000);
	LCD_Clear(LCD_COLOR_WHITE);
	delay(1000);
	LCD_Clear(LCD_COLOR_GREY);
	delay(1000);
	LCD_Clear(LCD_COLOR_BLUE2);
	delay(1000);
	LCD_Clear(LCD_COLOR_MAGENTA);

	LCD_Fill(20,20,100,100,LCD_COLOR_RED);
	LCD_DrawRectangle(120,20,220,100,LCD_COLOR_YELLOW);
	LCD_ShowString(20,180,200,16,16,(u8 *)"Hello This is LCD Test");

	return 1;
}
	

static  const xCommandLineInput LCD_command1 = {
	(const RAW_S8 *)"lcd_test",
	(const RAW_S8 *)"lcd_test:      test lcd display.\r\n",
	lcd_test,
	0
};

// lcd����ע������
void lcd_command_register()
{
	rsh_register_command(&LCD_command1, &pxNewListItem1);	
}
