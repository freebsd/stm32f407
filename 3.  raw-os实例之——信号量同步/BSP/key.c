/*
			KEY 驱动函数
			
			说明: KEY0,KEY1,KEY2按键按下产生下降沿中断
				  KEY_UP按下产生上升沿中断
*/

#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "raw_api.h"

// 定义一个信号量,用于按键中断与任务同步
RAW_SEMAPHORE	Key_Semaphore;

/**********************************************************************
* 函数名称： KEY_Init
* 功能描述： KEY GPIO 配置
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void KEY_Init()
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	GPIO_InitTypeDef   GPIO_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,  ENABLE); 					// 使能GPIOA时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,  ENABLE); 					// 使能GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE); 					// 使能SYSCFG时钟

	// PA0对应KEY_UP键
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;						// 浮空输入  
	GPIO_Init(GPIOA, &GPIO_InitStructure);  
	
	// PE2,3,4分别接KEY2,KEY1,KEY0
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4;            
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;           					// 模拟输入
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;           					// 设置上拉 
	GPIO_Init(GPIOE, &GPIO_InitStructure);                

	// KEY_UP对应外部中断线0,KEY2,KEY1,KEY0分别对应外部中断线2,3,4
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOE, EXTI_PinSource2);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOE, EXTI_PinSource3);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOE, EXTI_PinSource4);
	
	// 配置中断线0
	EXTI_InitStructure.EXTI_Line 	= EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode 	= EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  		// 上升沿触发
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	// 配置中断线2,3,4
	EXTI_InitStructure.EXTI_Line 	= EXTI_Line2 | EXTI_Line3 | EXTI_Line4;                   
	EXTI_InitStructure.EXTI_Mode 	= EXTI_Mode_Interrupt;          // 配置中断模式
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;      	// 配置为下降沿触发
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;                    	// 配置中断线使能
	EXTI_Init(&EXTI_InitStructure);                              

	// 安装外部中断线0
	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;             
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00; 
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// 安装外部中断线2
	NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;             
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02; 
	NVIC_Init(&NVIC_InitStructure);
	
	// 安装外部中断线3
	NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;             
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03; 
	NVIC_Init(&NVIC_InitStructure);
	
	// 安装外部中断线4
	NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;             
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x04; 
	NVIC_Init(&NVIC_InitStructure);
	
	// 创建用于按键中断和任务同步的信号量
	raw_semaphore_create(&Key_Semaphore,(RAW_U8 *)"KEY_SEMAPHORE",0);

}


// 外部中断线0服务程序
void EXTI0_IRQHandler(void)
{ 
	raw_enter_interrupt();												// 进入中断
    
	if(EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
		// 释放信号量(唤醒阻塞在该信号量上的最高优先级任务)	
		raw_semaphore_put(&Key_Semaphore);								// 释放同步信号量
        EXTI_ClearITPendingBit(EXTI_Line0);
	}

	raw_finish_int();  
}	

// 外部中断线2服务程序
void EXTI2_IRQHandler(void)
{
	raw_enter_interrupt();												// 进入中断

	if(EXTI_GetITStatus(EXTI_Line2) != RESET)
	{
		raw_semaphore_put(&Key_Semaphore);								// 释放同步信号量
		EXTI_ClearITPendingBit(EXTI_Line2);
	}
	
	raw_finish_int();  													// 退出中断
}

// 外部中断线3服务程序
void EXTI3_IRQHandler(void)
{
	raw_enter_interrupt();												// 进入中断
	
	if(EXTI_GetITStatus(EXTI_Line3) != RESET)
	{
		// 释放信号量(唤醒阻塞在该信号量上的最高优先级任务)	
		raw_semaphore_put(&Key_Semaphore);								// 释放同步信号量
		EXTI_ClearITPendingBit(EXTI_Line3);
	}
	
	raw_finish_int();  													// 退出中断
}

// 外部中断线4服务程序
void EXTI4_IRQHandler(void)
{
	raw_enter_interrupt();												// 进入中断
	
	if(EXTI_GetITStatus(EXTI_Line4) != RESET)
	{
		// 释放信号量(唤醒所有阻塞在该信号量上的任务)	
		raw_semaphore_put_all(&Key_Semaphore);	
		EXTI_ClearITPendingBit(EXTI_Line4);
	}
	
	raw_finish_int();  													// 进入中断
}

