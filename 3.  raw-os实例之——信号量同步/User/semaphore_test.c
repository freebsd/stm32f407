/*
           中断与信号量同步 演示程序



*/

#include "raw_api.h"
#include "led.h"


// 任务栈大小
#define TASK_STACK_SIZE   			64

// 任务栈
PORT_STACK Task1_Stack[TASK_STACK_SIZE];
PORT_STACK Task2_Stack[TASK_STACK_SIZE];

// 任务对像
RAW_TASK_OBJ 		Task1;
RAW_TASK_OBJ 		Task2;


// 按键中断与任务同步用的信号量
extern RAW_SEMAPHORE	Key_Semaphore;

// 第一个任务的执行函数入口
void First_Task(void * pParam)
{
	while(1)
	{
		// 等待信号量(永远阻塞)
		raw_semaphore_get(&Key_Semaphore,RAW_WAIT_FOREVER);
		
		LED_Toggle(1);			// 翻转LED1
		
	}
}


// 第二个任务的执行函数入口
void Second_Task(void * pParam)
{
	while(1)
	{
		// 等待信号量(永远阻塞)
		raw_semaphore_get(&Key_Semaphore,RAW_WAIT_FOREVER);
		
		LED_Toggle(2);		// 翻转LED2
	}
}

// 创建两个任务用于演示
void Semaphore_Test()
{
    
	// 创建第一个任务
	raw_task_create(&Task1, (RAW_U8  *)"task1", 0,
	                         7, 0,  Task1_Stack, 
	                         TASK_STACK_SIZE , First_Task, 1); 
	
	// 创建第二个任务
	raw_task_create(&Task2, (RAW_U8  *)"task2", 0,
	                         8, 0,  Task2_Stack, 
	                         TASK_STACK_SIZE , Second_Task, 1);  
}

