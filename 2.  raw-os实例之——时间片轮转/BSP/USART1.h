
#ifndef _USART1_H
#define _USART1_H

/* USART1 初始化 */ 
void USART1_Init(unsigned int BaudRate);

/* USART1 接收一个字节 */ 
void USART1_Receive_Char(unsigned char * byte);

/* USART1 发送一个字符 */ 
void USART1_Send_Char(unsigned char byte);

/* USART1 发送一个字符串 */ 
void USART1_Send_String(char *str);


#endif
