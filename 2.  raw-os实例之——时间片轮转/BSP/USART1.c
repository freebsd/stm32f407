/*
			STM32F407 USART1 驱动
			

*/
#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_usart.h"
#include "misc.h"
#include "raw_api.h"


/**********************************************************************
* 函数名称： USART1_GPIO_Config
* 功能描述： USART1 GPIO 配置
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void USART1_GPIO_Config()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	/* 1. 开时钟 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* 2. 配置GPIO,重映射PA9,10 -> USART1 */ 
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9,  GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}

/**********************************************************************
* 函数名称： USART1_NVIC_Config
* 功能描述： 配置USART1 NVIC 中断
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void USART1_NVIC_Config()
{
	NVIC_InitTypeDef NVIC_InitStructure;
	
	/* 设置优先级分组 */ 
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;					/* 选择串口1中断通道 */
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;				
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;	

	NVIC_Init(&NVIC_InitStructure);
}


/**********************************************************************
* 函数名称： USART1_Init
* 功能描述： USART1 初始化
* 输入参数： BaudRate —— 波特率
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void USART1_Init(u32 BaudRate)
{
	USART_InitTypeDef USART_InitStructure;
	
	/* 1. 配置GPIO用于USART1 */
	USART1_GPIO_Config();
	
	/* 2. 开USART1时钟 */ 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);	
	
	/* 3. 配置USART参数 */ 
	USART_InitStructure.USART_BaudRate = BaudRate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);	
	
	USART_ClearFlag(USART1,USART_IT_TXE);
	
	/* 4. USART1接收中断使能,配置NVIC,使能USART1 */ 
	USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
	
	USART1_NVIC_Config();

	USART_Cmd(USART1, ENABLE);	

}

/* USART1 接收一个字节 */ 
void USART1_Receive_Char(unsigned char * byte)
{	
	/* 读取接收到的字符 */ 
	*byte = USART_ReceiveData(USART1);
	
}

/* USART1 接收中断服务函数 */ 
void USART1_IRQHandler(void)
{	
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  	/* 判断是否为USART1接收中断 */
	{		
		USART_ClearITPendingBit(USART1,USART_IT_RXNE);		/* 清除中断标志 */
		
								
	} 	

}


/* USART1 发送一个字符 */ 
void USART1_Send_Char(unsigned char byte)
{
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
	
	USART_SendData(USART1,byte);
	
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);//等待数据发送完毕
	
}

/* USART1 发送一个字符串 */ 
void USART1_Send_String(char *str)
{
	// 解决第一个字符打印不出问题
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
	
	while(*str != '\0')
	{			
		USART_SendData(USART1,*(str ++));
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET); //等待数据发送完毕
	}
}
