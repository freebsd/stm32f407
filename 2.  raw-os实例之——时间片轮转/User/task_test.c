/*
            任务创建演示程序



*/

#include "raw_api.h"
#include "led.h"
#include "usart1.h"

// 任务栈大小
#define TASK_STACK_SIZE   			64

// 任务栈
PORT_STACK Task1_Stack[TASK_STACK_SIZE];
PORT_STACK Task2_Stack[TASK_STACK_SIZE];

// 任务对像
RAW_TASK_OBJ 		Task1;
RAW_TASK_OBJ 		Task2;


// 第一个任务的函数入口
void First_Task(void * pParam)
{
	int i;
	for( i = 0; i < 10; i ++)
	{
		LED_Toggle(1);      // 翻转LED1
		USART1_Send_String("\r\nThis is First Task!\r\n");
	}
}

// 第二个任务的函数入口
void Second_Task(void * pParam)
{
	int i;
	for( i = 0; i < 10; i ++)
	{
		LED_Toggle(2);      // 翻转LED2
		USART1_Send_String("\r\nThis is Second Task!\r\n");
	}

}

// 创建两个任务用于演示
void Task_Test()
{
    
	// 创建第一个任务,优先级8,时间片默认50个tick
	raw_task_create(&Task1, (RAW_U8  *)"task1", 0,
	                         8, 1,  Task1_Stack, 
	                         TASK_STACK_SIZE , First_Task, 1); 

	// 创建第二个任务,优先级8,时间片默认50个tick
	raw_task_create(&Task2, (RAW_U8  *)"task2", 0,
	                         8, 1,  Task2_Stack, 
	                         TASK_STACK_SIZE , Second_Task, 1); 
}

