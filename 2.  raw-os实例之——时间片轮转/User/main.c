/*
			RAW-OS 实例 —— 同优先级任务 时间片轮转
			
			STM32F4 固件库版本：V 1.3
			RAW-OS  内核版本：  V 1.056

*/

#include <stdio.h>
#include <stdarg.h>
#include <stm32f4xx_conf.h>
#include "raw_api.h"
#include "led.h"
#include "usart1.h"
#include "task_test.h"


// 启动任务的栈，任务对像定义
#define             INIT_TASK_STK_SIZE      256
PORT_STACK          Init_Task_Stack[INIT_TASK_STK_SIZE];
RAW_TASK_OBJ 		Init_Task;

// 启动任务执行函数声明
void Init_Task_Func(void *parg);

/**********************************************************************
* 函数名称： OS_CPU_SysTickInit
* 功能描述： RAW-OS 系统时钟初始化(STM32F407主频168MHz)
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void  OS_CPU_SysTickInit(void)
{
	/*
		配置SysTick中断频率100次/秒,故RAW-OS的系统周期10ms	
	*/
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}




/*************** 主函数 **********************/
int main()
{	
	SystemInit();								// 配置CPU时钟

	raw_os_init();								// 操作系统初始化
	
	// 创建启动任务,任务的优先级为1,配置系统硬件,创建其它任务
	raw_task_create(&Init_Task, (RAW_U8  *)"Init_Task", 0,
	                         1, 0,  Init_Task_Stack, 
	                         INIT_TASK_STK_SIZE , Init_Task_Func, 1); 
	
	raw_os_start();								// 启动操作系统
	
	return 0;	
}


// 启动任务的执行函数
void Init_Task_Func(void *parg)
{
	// 1. Initial all the used Hardware	
	OS_CPU_SysTickInit();						// 配置系统时钟
	USART1_Init(115200);                        // 初始化USART1,波特率115200
	LED_Init();									// LED初始化

    // 2. Create other Task
    Task_Test();
   
	raw_task_delete(raw_task_identify());		// 删除自已

}

