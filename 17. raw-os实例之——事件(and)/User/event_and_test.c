/*
		raw-event and 演示	

*/

#include <stdio.h>
#include <stdarg.h>
#include <stm32f4xx_conf.h>
#include <lib_string.h>
#include "raw_api.h"
#include "shell_task.h"
#include "USART1.h"
#include "led.h"


#define TASK_STACK_SIZE   			256		// 任务栈大小

PORT_STACK Task1_Stack[TASK_STACK_SIZE];	// 任务栈
PORT_STACK Task2_Stack[TASK_STACK_SIZE];
PORT_STACK Task3_Stack[TASK_STACK_SIZE];

RAW_TASK_OBJ 		Task1;					// 任务对像
RAW_TASK_OBJ 		Task2;
RAW_TASK_OBJ 		Task3;


RAW_U32  event_flag;           
extern RAW_EVENT Key_Event;                 // key.c中定义
	
	
// 第一个任务的执行函数入口
void First_Task(void * pParam)
{		
	
	while(1)
	{
		// 阻塞等待事件的BIT2,如果事件的BIT2置位(按键2按下),则此任务会被唤醒
		raw_event_get(&Key_Event, 0x2, RAW_AND_CLEAR,  &event_flag, RAW_WAIT_FOREVER);
        
        LED_Toggle(2);
        
        Uart_Printf("Task1 Receive Event BIT2,the event flag Value is: 0X%X\r\n",event_flag);
	}
}

// 第二个任务的执行函数入口
void Second_Task(void * pParam)
{
	
	while(1)
	{
        // 阻塞等待事件的BIT1,如果事件的BIT1置位(按键1按下),则此任务会被唤醒
		raw_event_get(&Key_Event, 0x1, RAW_AND_CLEAR,  &event_flag, RAW_WAIT_FOREVER);
        
        LED_Toggle(1);
        
        Uart_Printf("Task2 Receive Event BIT1,The Event Flag Value is: 0X%X\r\n",event_flag);
		
	}
}

// 第二个任务的执行函数入口
void Thrid_Task(void * pParam)
{

	while(1)
	{
		// 阻塞等待事件的BIT3和BIT2,只有在两个BIT同时置位时,任务才会被唤醒
		raw_event_get(&Key_Event, 0x6, RAW_AND_CLEAR,  &event_flag, RAW_WAIT_FOREVER);
        
        LED_Toggle(1);
        LED_Toggle(2);
        
        Uart_Printf("Task3 Receive Event BIT3 And BIT2,The Event Flag Value is: 0X%X\r\n",event_flag);
	}
}


void Event_And_Test()
{
	
	// 创建第一个任务
	raw_task_create(&Task1, (RAW_U8  *)"task1", 0,
	                         8, 0,  Task1_Stack, 
	                         TASK_STACK_SIZE , First_Task, 1); 

	// 创建第二个任务
	raw_task_create(&Task2, (RAW_U8  *)"task2", 0,
	                         9, 0,  Task2_Stack, 
	                         TASK_STACK_SIZE , Second_Task, 1); 
	
	// 创建第三个任务
	raw_task_create(&Task3, (RAW_U8  *)"task3", 0,
	                         10, 0,  Task3_Stack, 
	                         TASK_STACK_SIZE , Thrid_Task, 1); 
	
}

