/*
			STM32F407  LED 驱动
			
*/

#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "led.h"

/**********************************************************************
* 函数名称： LED_Init
* 功能描述： LED GPIO 配置
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void LED_Init()
{
  GPIO_InitTypeDef GPIO_InitStructure;
	
	/* Enable LED GPIO Clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);

  /* Configure the GPIO_LED pin */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOF, &GPIO_InitStructure);
	GPIO_SetBits(GPIOF,GPIO_Pin_9 | GPIO_Pin_10);
	
}

/* Turn OFF the LED */
void LED_OFF(unsigned char led_number)
{
  switch (led_number)
	{
		case 1:
			GPIO_SetBits(GPIOF,GPIO_Pin_9);
			break;
		case 2:
			GPIO_SetBits(GPIOF,GPIO_Pin_10);
			break;
	}		
}

/* Turn ON the LED */
void LED_ON(unsigned char led_number)
{
  switch (led_number)
	{
		case 1:
			GPIO_ResetBits(GPIOF,GPIO_Pin_9);
			break;
		case 2:
			GPIO_ResetBits(GPIOF,GPIO_Pin_10);
			break;
	}		  
}
/* Toggle the LED */
void LED_Toggle(unsigned char led_number)
{
  switch(led_number)
	{
		case 1:
			GPIOF->ODR ^= GPIO_Pin_9;
		  break;
		case 2:
			GPIOF->ODR ^= GPIO_Pin_10;
		  break;
	}	
}
