
#ifndef _LED_H
#define _LED_H

void LED_Init(void);
void LED_ON(unsigned char led_number);
void LED_OFF(unsigned char led_number);
void LED_Toggle(unsigned char led_number);

#endif


