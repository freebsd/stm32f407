/*
		shell led control command
			
		eg: led 1 1 ——turn on the LED1
				led 1 0 ——turn off the LED1
				
		eg: led-test: create the led water task
*/
#include "stdio.h"
#include "raw_api.h"
#include "rsh.h"
#include "lib_string.h"
#include "common.h"
#include "led.h"

// 定义命令结点
static xCommandLineInputListItem pxNewListItem1;		// LED x x命令
static xCommandLineInputListItem pxNewListItem2;		// LED water
static xCommandLineInputListItem pxNewListItem3;		// LED start
static xCommandLineInputListItem pxNewListItem4;		// LED stop

// define LED Task STACK SIZE
#define LED_TASK_STACK_SIZE						128

// define LED Task STACK and TASK OBJ
PORT_STACK		LED_Task_Stack[LED_TASK_STACK_SIZE];
RAW_TASK_OBJ	LED_Task;

// Shell 中LED测试命令回调函数
RAW_S32 led_test(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, RAW_S8 * pcCommandString)
{
	RAW_S8 *pParameter;						// 指向参数(字符串)的指针
	RAW_U8 para[10]={0};					// 获取的参数(字符串)缓存
	RAW_S32 paraStringLength;			// 参数长度	
	
	RAW_U32 para1,para2;					// 两个参数
	
	// 获得第一个参数	
	pParameter = (RAW_S8 *)rsh_get_parameter
												(pcCommandString,			// the input command string
												1,										// the parameter number
												&paraStringLength);		// the parameter length
	
	raw_memcpy(para,pParameter,paraStringLength);
	para[paraStringLength]='\0';
	
	// 将第一个参数(字符串)转为整形
	str_to_int(para,&para1);
	
	// 获得第二个参数
	pParameter = (RAW_S8 *)rsh_get_parameter(pcCommandString,2,&paraStringLength);
	raw_memcpy(para,pParameter,paraStringLength);
	para[paraStringLength]='\0';
	
	// 将第二个参数(字符串)转为整形
	str_to_int(para,&para2);
	
	switch(para1)
	{
		case 1:
			switch(para2)
			{
				case 1: LED_ON(1); break;
			  case 0: LED_OFF(1);break;
				default:raw_strcat((char *)pcWriteBuffer,"para2 is error(0 or 1)\t");break;
			}
			break;
		case 2:
			switch(para2)
			{
				case 0: LED_OFF(2);break;	
				case 1: LED_ON(2);break;
			  
				default:raw_strcat((char *)pcWriteBuffer,"para2 is error(0 or 1)\t");break;
			}
			break;		
		default:raw_strcat((char *)pcWriteBuffer,"para1 is error(1,2)\t");break;
	}
	
	return 1;									// 必须返回1
}

// LED_Task_Function
void LED_Task_Fun(void *parg)
{
	while(1)
	{
		LED_Toggle(1);
		raw_sleep(50);
		LED_Toggle(2);
		raw_sleep(50);
	}
}

// LED water task 命令体定义 
RAW_S32 led_water(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, RAW_S8 * pcCommandString)
{
	raw_task_create(&LED_Task,(RAW_U8 *)"LED_Task",0,
									CONFIG_RAW_PRIO_MAX-4,0,
									LED_Task_Stack,
									LED_TASK_STACK_SIZE,
								  LED_Task_Fun,1);
	
	return 1;																	// must return 1
}

// LED stop 命令体定义 
RAW_S32 led_stop(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, RAW_S8 * pcCommandString)
{
	raw_task_suspend(&LED_Task);
	
	return 1;																	// must return 1
}

// LED start 命令体定义 
RAW_S32 led_start(RAW_S8 *pcWriteBuffer, RAW_U32 xWriteBufferLen, RAW_S8 * pcCommandString)
{
	raw_task_resume(&LED_Task);
	
	return 1;																	// must return 1
}

// led 开关,命令体定义
static  const xCommandLineInput led_command1 = {
	(const RAW_S8 *)"led",
	(const RAW_S8 *)"led:           <led number> <on or off> led number(1,2),on(1),off(0).",
	(pdCOMMAND_LINE_CALLBACK)led_test,
	2
};

// led 开关,命令体定义
static  const xCommandLineInput led_command2 = {
	(const RAW_S8 *)"led_water",
	(const RAW_S8 *)"led_water:     Create led water task.",
	(pdCOMMAND_LINE_CALLBACK)led_water,
	0
};

// led stop,命令体定义
static  const xCommandLineInput led_command3 = {
	(const RAW_S8 *)"led_stop",
	(const RAW_S8 *)"led_stop:      stop   led water task.",
	(pdCOMMAND_LINE_CALLBACK)led_stop,
	0
};

// led start,命令体定义
static  const xCommandLineInput led_command4 = {
	(const RAW_S8 *)"led_start",
	(const RAW_S8 *)"led_start:     start  led water task.",
	(pdCOMMAND_LINE_CALLBACK)led_start,
	0
};

// 注册命令
void led_command_register()
{
	// 注册控制硬件的命令
	rsh_register_command(&led_command1, &pxNewListItem1);
	rsh_register_command(&led_command2, &pxNewListItem2);
	rsh_register_command(&led_command3, &pxNewListItem3);
	rsh_register_command(&led_command4, &pxNewListItem4);
}

