/*
		raw os shell task 
		
		note: task priority is only higher than idle task and stat task

*/
#include "raw_api.h"
#include "rsh.h"
#include "lib_string.h"

#include "USART1.h"									// 底层UART驱动


// 重定向USART Input/Output Function 
#define USART_Send_Char		USART1_Send_Char
#define USART_Send_String	USART1_Send_String
#define USART_Receive_Char  USART1_Receive_Char

// define Shell Task stack size 
#define SHELL_TASK_STACK_SIZE			256

// define shell task stack and task obj 
PORT_STACK Shell_Task_Stack[SHELL_TASK_STACK_SIZE];
RAW_TASK_OBJ Shell_Task;

// ENTER KEY and BACKSPACE KEY ASCII  
#define ENTER_KEY 		0x0d
#define BACKSPACE_KEY 	'\b'


// input buffer and output buffer 
static RAW_S8 input_buffer[256];
static RAW_S8 output_buffer[1024];

// print a enter key
void USART_Send_Enter()
{
	USART_Send_Char('\r');
	USART_Send_Char('\n');
}

// Shell Task Execute Function
void Shell_Task_Func(void *parg)
{
	RAW_U8 ch; 
	RAW_U8 i ;	
	RAW_U8 ret;
	RAW_U8 *raw_os = (RAW_U8 *)"raw-os#";	
	
	while(1)
	{
		i = 0;
		USART_Send_String(raw_os);			// 打印raw-os#
		
		while(1)
		{
			// Receive the char(waiting the sync semaphore)
			USART_Receive_Char(&ch);
			if(ch == ENTER_KEY)				// input enter key
			{
				input_buffer[i] = '\0';
				break;
			}
			else if(ch == BACKSPACE_KEY)	// input  backspace key
			{
				if(i > 0)
				{
					i --;		
					input_buffer[i] = '\0';
					
					USART_Send_Char('\b');
					USART_Send_Char(' ');
					USART_Send_Char('\b');
				}
			}
			else							// input valid char
			{
				input_buffer[i ++] = ch;	// copy in input_buffer
				USART_Send_Char(ch);
			}			
		}		
		// print a enter 
		USART_Send_Enter();
		
		if(input_buffer[0] != '\0')
		{
			// 清空输出缓冲区
			raw_memset(output_buffer,0,1024);
			
			do
			{				
				// 命令处理
				ret = rsh_process_command(input_buffer,output_buffer,1024);
				
				// 打印处理结果
				USART_Send_String((RAW_U8 *)output_buffer);
				
				USART_Send_Enter(); 	// 回册换行
				
			}while(ret == 0);
		}		
	}
}

/**********************************************************************
* 函数名称： Shell_Task_Init
* 功能描述： 初始化Shell任务,并注册所有的shell命令
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void Shell_Task_Init()
{
	
	// Shell Task Create
	raw_task_create(&Shell_Task,(RAW_U8 *)"Shell Task",0,
					CONFIG_RAW_PRIO_MAX-3,0,		// 优先级仅高于空闲和统计任务
					Shell_Task_Stack,SHELL_TASK_STACK_SIZE,
					Shell_Task_Func,1);		
	
}


