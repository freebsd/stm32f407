/*
        Block内存管理 演示
        
        注：分配10K的空间给block_pool管理,每个内存块是1K


*/

#include <stdio.h>
#include <lib_string.h>
#include "raw_api.h"
#include "shell_task.h"
#include "USART1.h"


#define TASK_STACK_SIZE   			256		// 任务栈大小

PORT_STACK Task1_Stack[TASK_STACK_SIZE];	// 任务栈
PORT_STACK Task2_Stack[TASK_STACK_SIZE];
PORT_STACK Task3_Stack[TASK_STACK_SIZE];

RAW_TASK_OBJ 		Task1;					// 任务对像
RAW_TASK_OBJ 		Task2;
RAW_TASK_OBJ 		Task3;


#define RAW_BLOCK_SIZE	1024 * 10			// block内存大小 10K
unsigned char raw_block_pool[RAW_BLOCK_SIZE];	

MEM_POOL block_pool;						// 定义一个内存池,用来管理block类型的内存块

// 以下四个变量在key.c中定义
extern RAW_SEMAPHORE	Key0_Semaphore;		// 按键0同步信号量
extern RAW_SEMAPHORE	Key1_Semaphore;		// 按键1同步信号量
extern RAW_SEMAPHORE	Key2_Semaphore;		// 按键2同步信号量
extern RAW_TIMER      	Key_Timer;			// 周期性检测按键定时器
extern RAW_U16			Key_Timer_Function(RAW_VOID *expiration_input);// 定时器回调函数

void * block_ptr[20] = {NULL};				// 指针数组,用于存储内存block块地址


// 第一个任务的执行函数入口
void First_Task(void * pParam)
{		
	int ret;
	int i = 0;
    
	while(1)
	{
		// 阻塞等待按键2信号量
		raw_semaphore_get(&Key2_Semaphore,RAW_WAIT_FOREVER);
				
		// 按键按下,则分配内存块
		ret = raw_block_allocate(&block_pool, (RAW_VOID **)&block_ptr[i]);
		if ( RAW_NO_MEMORY == ret ) 			// 分配失败
		{
			Uart_Printf("block pool no memory to be allocated!\r\n");			
		}
		else									// 分配成功
		{
			// 打印分配的内存地址
			Uart_Printf("block allocated success! block number: %d, block ptr: %p\r\n",i, block_ptr[i]) ;
			i ++;
			
			if(i >= 20)
			{
				i = 0;
			}
		}
	}
}

// 第二个任务的执行函数入口
void Second_Task(void * pParam)
{
	int i = 0;
	int ret;
	
	while(1)
	{
		// 阻塞等待按键1信号量
		raw_semaphore_get(&Key1_Semaphore,RAW_WAIT_FOREVER);
		
		ret = raw_block_release(&block_pool, (RAW_VOID *)block_ptr[i]);
		
		if(RAW_SUCCESS != ret)
		{
			Uart_Printf("block release error!,block number: %d,block ptr: %p\r\n",i,block_ptr[i]);
		}
		else
		{
			block_ptr[i] = NULL;			// 释放后要置NULL
			Uart_Printf("block release success!,block number: %d,block ptr: %p\r\n",i,block_ptr[i]);
		}
		
		i ++;
	}
}

// 第二个任务的执行函数入口
void Thrid_Task(void * pParam)
{
	int i;
    int ret;

	
	while(1)
	{
		// 阻塞等待按键0信号量
		raw_semaphore_get(&Key0_Semaphore,RAW_WAIT_FOREVER);
		
		for(i = 0; i < 20; i ++)		// 遍历
		{
			if( block_ptr[i] != NULL )	// 有效内存,则释放
			{			
				ret = raw_block_release(&block_pool, (RAW_VOID *)block_ptr[i]);
				if(RAW_SUCCESS != ret)
				{
					Uart_Printf("block release error!,block number: %d,block ptr: %p\r\n",i,block_ptr[i]);
				}
				else
				{
					block_ptr[i] = NULL;			// 释放后要置NULL
					Uart_Printf("block release success!,block number: %d,block ptr: %p\r\n",i,block_ptr[i]);
				}

			}
		}
	}
}


/******************** BLOCK内存管理测试 **********************/
void RAW_Block_Test()
{
    int ret;
    
    // 创建第一个任务
	raw_task_create(&Task1, (RAW_U8  *)"task1", 0,
	                8, 0,  Task1_Stack, 
	                TASK_STACK_SIZE , First_Task, 1); 

	// 创建第二个任务
	raw_task_create(&Task2, (RAW_U8  *)"task2", 0,
	                9, 0,  Task2_Stack, 
	                TASK_STACK_SIZE , Second_Task, 1); 
	
	// 创建第三个任务
	raw_task_create(&Task3, (RAW_U8  *)"task3", 0,
	                10, 0,  Task3_Stack, 
	                TASK_STACK_SIZE , Thrid_Task, 1); 
    

	// 创建一个block类型的内存池,block大小为4字节,内存总大小10KB	
	ret = raw_block_pool_create(&block_pool, (RAW_U8  *)"block_pool", 1024, raw_block_pool, RAW_BLOCK_SIZE);
	
	if(RAW_SUCCESS == ret)				// 创建成功
	{
		Uart_Printf("block pool create success!\r\n");
	}
	else								// 创建失败
	{	
		switch(ret)
		{
			case RAW_INVALID_ALIGN:		
				Uart_Printf("pool_start or pool_size isn't 4 bytes aligned!\r\n");
				break;
			case RAW_BLOCK_SIZE_ERROR:	
				Uart_Printf("pool_size isn't  twice as much as the block_size!\r\n",ret);
				break;		
		}		
	}
}
