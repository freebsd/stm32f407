/*
            多任务环境下 临界区资源 测试
            
       
       LCD设备作为一种临界资源,可能被多个任务抢占式使用,这样就了现使用混乱的情况
        


*/

#include <stdio.h>
#include <lib_string.h>
#include "raw_api.h"

#include "lcd.h"

// 任务栈大小
#define TASK_STACK_SIZE   			128

// 任务栈
PORT_STACK Task1_Stack[TASK_STACK_SIZE];
PORT_STACK Task2_Stack[TASK_STACK_SIZE];

// 任务对像
RAW_TASK_OBJ 		Task1;
RAW_TASK_OBJ 		Task2;

static void delay_ms(int count)
{
	unsigned int i,j;
	while(count --)
	{
		for(i = 0; i < 1000; i ++)
		{
			for(j = 0; j < 168; j ++);		
		}
	}
}

// 第一个任务的函数入口
void First_Task(void * pParam)
{		
	while(1)
	{
		// 优先级高,2s抢占一次LCD资源
		LCD_Clear(LCD_COLOR_MAGENTA);
		LCD_ShowString(20,180,200,16,16,(u8 *)"Hello This is First Task");
		raw_sleep(200);			
	}
}

// 第二个任务的函数入口
void Second_Task(void * pParam)
{
	char str[] = "This is Second Task: ";
	int  i = 0;
	
	while(1)
	{
		// 优先级低,在使用LCD过程中可能会被高优先级的任务2抢占
		while(++i < 10)
		{
			LCD_Clear(LCD_COLOR_GREY);
			str[sizeof(str) - 2] = i + 0x30; 			// 打印执行次数
			LCD_ShowString(20,200,200,16,16,(u8 *)str);
			delay_ms(200);								/* 模拟LCD被长时间占用 */			
		}
		if(i == 10)								
		{
			i = 0;
		}
		raw_sleep(100);
	}
}


void critical_region_test()
{

    // 创建第一个任务
	raw_task_create(&Task1, (RAW_U8  *)"task1", 0,
	                         10, 0,  Task1_Stack, 
	                         TASK_STACK_SIZE , First_Task, 1); 

	// 创建第二个任务
	raw_task_create(&Task2, (RAW_U8  *)"task2", 0,
	                         11, 0,  Task2_Stack, 
	                         TASK_STACK_SIZE , Second_Task, 1); 

}
