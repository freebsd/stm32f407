/*
		实验说明：  采用mutex保护临界资源
        (mutex解决优先级反转的方式为优先级继承)
        
		
		实验现像：  Task1和Task2交替使用LCD来显示自已的信息
						
		STM32F4 固件库版本：V 1.3
		RAW-OS  内核版本：  V 1.056		

*/

#include <stdio.h>
#include <stdarg.h>
#include <stm32f4xx_conf.h>
#include <lib_string.h>
#include "raw_api.h"
#include "shell_task.h"
#include "lcd.h"
#include "led.h"									// LED硬件操作相关
#include "usart1.h"
#include "led_cmd.h"								// LED控制命令接口
#include "system_cmd.h"								// 系统命令
#include "task_stack_detect_cmd.h"					// 栈检测命令
#include "mutex_test.h"


// 启动任务栈,任务对像初始化
#define             INIT_TASK_STK_SIZE      256
PORT_STACK          Init_Task_Stack[INIT_TASK_STK_SIZE];
RAW_TASK_OBJ 		Init_Task;

// 启动任务执行函数
void Init_Task_Func(void *parg);


/**********************************************************************
* 函数名称： OS_CPU_SysTickInit
* 功能描述： RAW-OS 系统时钟初始化(STM32F407主频168MHz)
* 输入参数： 无
* 输出参数： 无
* 返 回 值： 无
***********************************************************************/
void  OS_CPU_SysTickInit(void)
{
	/*
		配置SysTick中断步率100次/秒,故RAW-OS的系统周期10ms	
	*/
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}



/*************** 主函数 **********************/
int main()
{	
	SystemInit();
	
	raw_os_init();								// 操作系统初始化
	

	// 创建启动任务,配置系统硬件,创建其它任务
	raw_task_create(&Init_Task, (RAW_U8  *)"Init_Task", 0,
	                1, 0,  Init_Task_Stack, 
	                INIT_TASK_STK_SIZE , Init_Task_Func, 1); 
	
	Shell_Task_Init();							// shell任务初始化
	
	raw_os_start();								// 启动操作系统
	
	return 0;	
}

// 启动任务的执行函数
void Init_Task_Func(void *parg)
{
	// 1. Initial all the used Hardware	
	OS_CPU_SysTickInit();						// 配置系统时钟
	
	USART1_Init(115200);                        // 配置USART1
	LED_Init();									// LED初始化    
	LCD_Init();									// LCD初始化

	// 2. Register all the Commands
	system_command_Register();                  // 注册系统命令
	register_task_stack_command();              // 注册系统任务栈检测命令
    led_command_register();                     // 注册LED控制命令
    
    // 3. create other tasks
    Mutex_Test();
    
	Shell_Task_Init();							// shell任务初始化	
	raw_task_delete(raw_task_identify());		// 删除

}



